Void listfunc([t] x)
{
	return;
}

Int main()
{
	[Int] x = 1 : 2 : [];
	listfunc(0 : x);
	listfunc(True : x);
	return 0;
}
