/*
Three ways to implement the factorial function in SPL.
First the recursive version .
*/

Int facR ( Int n )
{
    if ( n < 2 )
        return 1;
    else
        return n * facR (n - 1);
}

// The iterative version of the factorial function
Int facI ( Int n )
{
    Int r = 1 ;
    while ( n > 1 )
    {
        r = r * n;
        n = n - 1;
    }
    return r;
}
// A main function to check the results
Int main ()
{
	Int n = 0;
    Bool ok = True;
	
	print(n < 5);
	n = n + 1;
	print(n < 5); 
	while ( n < 5 )
    {
		print(n);
        n = n + 1;
    }
	print(ok);
	return 0;
}

Int product ( [ Int ] list )
{
    if ( isEmpty ( list ))
        return 1;
    else
        return head ( list ) * product ( tail ( list ) ) ;
}

// Generates a list of integers from the first to the last argument
[ Int ] fromTo ( Int from , Int to )
{
    if ( from <= to )
        return from : fromTo ( from + 1 , to ) ;
    else
        return [ ] ;
}


// A list based factorial function
// Defined here to show that functions can be given in any order (unlike C)
Int facL ( Int n )
{
    return product (fromTo ( 1 , n ) ) ;
}

