from type import *
from context import Context
from copy import deepcopy
from colorama import Fore

class SemanticAnalysis:
    class TypeException(Exception):
        def __init__(self, expected, found, token):
            self._expected = expected
            if found:
                self._found = found
            else:
                self._found = "unknown type"
            
            if token:
                self._token = token
            else:
                self._token = ""
            
        def __str__(self):
            return "Types not matching: expected {0} but found {1} ".format(str(self._expected), str(self._found)) + str(self._token)
        def __repr__(self):
            return self.__str__()
        
    def __init__(self):
        self.errored = False
    
    def checkProg(self, node):
        context = Context()
        context.addPredefineds()
        self._addFuncDeclsToContext(node, context)
        
        for decl in node.children:
            self._checkDecl(decl, context)
            
        return not self.errored
    
    def _checkDecl(self, node, context):
        try:
            if node.type == "FuncDecl":
                self._checkFuncDecl(node, context)
            elif node.type == "VarDecl":
                self._checkVarDecl(node, context)
            else:
                raise ValueError()
        except Context.MultiplyDefinedException as e:
            self.errored = True
            print Fore.RED + "Identifier \"{0}\" was multiply defined at {1}:{2}".format(e.id, node.token.line, node.token.column) + Fore.RESET
            print Fore.GREEN + "Note: using earlier definition." + Fore.RESET
        except Context.UndefinedIdException as e:
            self.errored = True
            print Fore.RED + "Usage of undeclared id: \"{0}\" at {1}:{2}".format(e.id, node.token.line, node.token.column) + Fore.RESET
        except SemanticAnalysis.TypeException as e:
            self.errored = True
            print e
    
    def _addFuncDeclsToContext(self, node, context):
        assert node.type == "Prog"
        try:
            for decl in node.children:
                if decl.type == "FuncDecl":
                    id = decl.children[1].children[0].type
                    context.declareType(id, self._getFuncType(decl, {}))
        except Context.MultiplyDefinedException as e:
            self.errored = True
            print Fore.RED + "Identifier \"{0}\" was multiply defined at {1}:{2}".format(e.id, decl.token.line, decl.token.column) + Fore.RESET
            print Fore.GREEN + "Note: using earlier definition." + Fore.RESET
        except Context.UndefinedIdException as e:
            self.errored = True
            print Fore.RED + "Usage of undeclared id: \"{0}\" at {1}:{2}".format(e.id, decl.token.line, decl.token.column) + Fore.RESET
            
    def _checkVarDecl(self, decl, context):
        assert decl.type == "VarDecl"
        # determine the declared type of the var
        varType = self._getType(decl.children[0], {})
        
        # check wheter the assignment matches the declared type
        self._checkExpType(decl.children[2], varType, context)
        
        context.declareType(decl.children[1].children[0].type, varType)
    
    def _checkFuncDecl(self, decl, context):
        assert decl.type == "FuncDecl"
        
        gtx = {}
        
        funcType = self._getFuncType(decl, gtx)
        functionArgs = self._getFunctionArgs(decl.children[2], gtx)
        functionContext = self._createFunctionContext(context, functionArgs)
        
        # type check var decls and add them to the context
        for varDecl in decl.children[3].children:
            self._checkVarDecl(varDecl, functionContext)
        
        # check the function body
        for statement in decl.children[4].children:
            self._checkStatement(statement, functionContext, funcType.returnType)
                
    def _checkStatement(self, nodeStatement, context, returnType):
        assert nodeStatement.type == "Stmt"
        #Stmt = '{' Stmt* '}'
        #| 'if' '(' Exp ')' Stmt [ 'else' Stmt ]
        #| 'while' '(' Exp ')' Stmt
        #| id '=' Exp ';'
        #| FunCall ';'
        #| 'return' [ Exp ] ';'
        if nodeStatement.children[0].type == "StmtBlock":
            for child in nodeStatement.children[0].children:
                self._checkStatement(child, context, returnType)
        elif nodeStatement.children[0].type == "IfStmt":
            # check whether the if part is of type bool
            self._checkExpType(nodeStatement.children[0].children[0], Type(Type.BOOL), context)
            
            # check the if block
            self._checkStatement(nodeStatement.children[0].children[1], context, returnType)
            
            # check else statement if present
            if nodeStatement.children[0].children[2].children:
                # else stmt is present
                self._checkStatement(nodeStatement.children[0].children[2].children[0].children[0], context, returnType)
        elif nodeStatement.children[0].type == "WhileStmt":
            # check whether the while condition is a bool
            self._checkExpType(nodeStatement.children[0].children[0], Type(Type.BOOL), context)
            
            # check the statement
            self._checkStatement(nodeStatement.children[0].children[1], context, returnType)
        elif nodeStatement.children[0].type == "Assignment":
            id = nodeStatement.children[0].children[0].children[0].type
            self._checkExpType(nodeStatement.children[0].children[1], context.getType(id), context)
        elif nodeStatement.children[0].type == "FunCall":
            # lookup the type of the function
            funcId = nodeStatement.children[0].children[0].children[0].type
            context = deepcopy(context)
            funcType = context.getType(funcId)
            
            # check the actual args
            actArgNodes = self._getActArgs(nodeStatement.children[0].children[1])
            self._checkActArgs(actArgNodes, funcType, context)
        elif nodeStatement.children[0].type == "ReturnStmt":
            if nodeStatement.children[0].children[0].children:
                self._checkExpType(nodeStatement.children[0].children[0].children[0], returnType, context)
            else:
                if not returnType == Type(Type.VOID):
                    raise SemanticAnalysis.TypeException(returnType, Type(Type.VOID), nodeStatement.children[0].token)
        else:
            # checkStatement not defined on given argument
            raise ValueError()
    
    def _getFuncType(self, node, gtx):
        retType = self._getRetType(node.children[0], gtx)
        args = self._getFunctionArgs(node.children[2], gtx)
        return FunctionType([type for (id, type) in args], retType)
            
    def _getRetType(self, node, gtx):
        assert node.type == "RetType"
        if node.children[0].type == "Void":
            return Type(Type.VOID)
        else:
            #the rettype node is a wrapper around the actual node type
            return self._getType(node.children[0], gtx)
    
    def _getType(self, node, gtx):
        if node.type == "Int":
            return Type(Type.INT)
        elif node.type == "Bool":
            return Type(Type.BOOL)
        elif node.type == "TypeId":
            id = node.children[0].children[0].type
            if not gtx.get(id, None):
                gtx[id] = GenericType(id)
            return gtx[id]
        elif node.type == "TypeList":
            return ListType(self._getType(node.children[0], gtx))
        elif node.type == "Tuple":
            return TupleType(self._getType(node.children[0], gtx), self._getType(node.children[1], gtx))
        else:
            raise ValueError()
    
    def _getFunctionArgs(self, node, gtx):
        assert node.type == "FArgs"
        if node.children:
            type = self._getType(node.children[0].children[0], gtx)
            id = node.children[0].children[1].children[0].type
            return [(id, type)] + self._getFunctionArgs(node.children[0].children[2], gtx)
        else:
            return []
    
    def _getActArgs(self, node):
        """ return a list of exp notes that form the act args of a funcall"""
        assert node.type == "ActArgs"
        if node.children:
            return [node.children[0].children[0]] + self._getActArgs(node.children[0].children[1])
        else:
            return []
    
    def _createFunctionContext(self, context, fArgs):
        """ create a context for the function block with given supercontext """
        ret = Context(context)
        for (id, type) in fArgs:
            ret.declareType(id, type)
            
        return ret
    
    def _checkActArgs(self, actArgNodes, funcType, context):
        """ check whether list of ActArg nodes matches the arguments of given funcType """
        if not len(actArgNodes) == len(funcType.argumentTypes):
            raise SemanticAnalysis.TypeException(funcType, None, None)
        for i in range(len(actArgNodes)):
            self._checkExpType(actArgNodes[i], funcType.argumentTypes[i], context)
    
    def _checkExpType(self, expNode, type, context):
        if not type:
            print "probleem!"
            
        """ check whether the exp is of the desired type """
        if expNode.type == "Exp" or expNode.type == "ExpPrime":
            # exp is a list
            if not type.unify(ListType(GenericType('z')), context):
                # if the desired type is not a listtype, raise an exception
                raise SemanticAnalysis.TypeException(type, ListType(GenericType('')), None)
            type = ListType(GenericType('z'))
            if len(expNode.children) > 1:
                # check the head of the list
                self._checkExpType(expNode.children[0], type.valueType, context)
                
                # check the rest of the list
                self._checkExpType(expNode.children[1], type, context)
            else:
                # the first child must be a list by itself (i.e. empty list)
                self._checkExpType(expNode.children[0], type, context)
            
        elif expNode.type == "Exp6" or expNode.type == "Exp5":
            # Exp6 = Exp5 Exp'*
            # Exp6' = '||' Exp5
            # Exp5 = Exp4 Exp5'*
            # Exp5' = '&&' Exp4
            # top level operator is || or &&, so this can only return a bool
            if not type.unify(Type(Type.BOOL), context):
                raise SemanticAnalysis.TypeException(type, Type(Type.BOOL), None)
            
            self._checkExpType(expNode.children[0], Type(Type.BOOL), context)
            for child in expNode.children[1].children:
                self._checkExpType(child.children[0], Type(Type.BOOL), context)

        elif expNode.type == "Exp4":
            # Exp4 = Exp3 Exp4'*
            # Exp4' = Op4 Exp3
            # operator is '==' | '<' | '>' | '<=' | '>=' | '!='
            # TODO define these operators on bools?
            # these operators return bools, and are defined on ints.
            if not type.unify(Type(Type.BOOL), context):
                raise SemanticAnalysis.TypeException(type, Type(Type.BOOL), None)
            
            # check whether child exps are ints
            self._checkExpType(expNode.children[0], Type(Type.INT), context)
            for child in expNode.children[1].children:
                self._checkExpType(child.children[1], Type(Type.INT), context)

        elif expNode.type == "Exp3" or expNode.type == "Exp2":
            # Exp3 = Exp2 Exp3'*
            # Exp3' = Op3 Exp2
            # Op3 = '+' | '-'
            # Exp2 = Exp1 Exp2'*
            # Exp2' = Op2 Exp1
            # Op2 = '*' | '/' | '%'
            # operator operates on ints and returns int
            if not type.unify(Type(Type.INT), context):
                raise SemanticAnalysis.TypeException(type, Type(Type.INT), None)

            self._checkExpType(expNode.children[0], Type(Type.INT), context)
            for child in expNode.children[1].children:
                self._checkExpType(child.children[1], Type(Type.INT), context)
                
        elif expNode.type == "Exp1":
            # Exp1 = FunCall
            # | id 
            # | Op1 Exp1
            # | int
            # | bool
            # | '(' Exp ')'
            # | '[]'
            # | '(' Exp ',' Exp ')'
            if expNode.children[0].type == "FunCall":
                context_copy = deepcopy(context)
                
                funcId = expNode.children[0].children[0].children[0].type
                funcType = context_copy.getType(funcId)
                
                if not isinstance(funcType, FunctionType):
                    raise SemanticAnalysis.TypeException(FunctionType([], type), funcType, None)
                
                # check ret type
                if not funcType.returnType.unify(type, context_copy):
                    raise SemanticAnalysis.TypeException(type, funcType.returnType, None)
                
                # check args
                actArgNodes = self._getActArgs(expNode.children[0].children[1])
                self._checkActArgs(actArgNodes, context_copy.getType(funcId), context_copy)
                
            elif expNode.children[0].type == "id":
                id = expNode.children[0].children[0].type
                if not context.isDefined(id):
                    raise Context.UndefinedIdException(id)
                varType = context.getType(id)
                if not varType.unify(type, context):
                    raise SemanticAnalysis.TypeException(type, varType, None)
                
            elif expNode.children[0].type == "Op1Exp":
                # this is an op1 exp
                opNode = expNode.children[0].children[0]
                expOneNode = expNode.children[0].children[1]
                if opNode.children[0].type == "!":
                    if not type.unify(Type(Type.BOOL), context):
                        raise SemanticAnalysis.TypeException(type, Type(Type.BOOL), opNode.token)

                    self._checkExpType(expOneNode, Type(Type.BOOL), context)
                else:
                    if not type.unify(Type(Type.INT), context):
                        raise SemanticAnalysis.TypeException(type, Type(Type.INT), opNode.token)

                    self._checkExpType(expOneNode, Type(Type.INT), context)
            elif expNode.children[0].type == "int":
                if not type.unify(Type(Type.INT), context):
                    raise SemanticAnalysis.TypeException(type, Type(Type.INT), expNode.children[0].children[0].token)
            elif expNode.children[0].type == "bool":
                if not type.unify(Type(Type.BOOL), context):
                    raise SemanticAnalysis.TypeException(type, Type(Type.BOOL), expNode.children[0].children[0].token)
            elif expNode.children[0].type == "ParenthesesExp":
                self._checkExpType(expNode.children[0], type, context)
            elif expNode.children[0].type == "EmptyList":
                if not isinstance(type, ListType):
                    raise SemanticAnalysis.TypeException(type, ListType(GenericType("")), expNode.children[0].token)
            elif expNode.children[0].type == "TupleExp":
                if not isinstance(type, TupleType):
                    raise SemanticAnalysis.TypeException(type, TupleType(GenericType(""), GenericType("")), None)
                
                self._checkExpType(expNode.children[0].children[0], type.left, context)
                self._checkExpType(expNode.children[0].children[1], type.right, context)
            else:
                raise ValueError("unknown type of Exp1: " + expNode.children[0].type)
        elif expNode.type == "ParenthesesExp":
            self._checkExpType(expNode.children[0], type, context)        
        else:
            raise ValueError("unknown exp type: " + expNode.type)
            
            