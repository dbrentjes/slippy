
from irnodes import *
from ssm_spl_builtins import SsmSplBuiltins

class SSMBackend:
    
    def __init__(self):
        self.byte_code = []
        self._byte_count = 0
        
        self.current_string = ""
        
        self._global_dict = {}
        self._local_dict = {}
        
    def process(self, ir):
        
        self.current_string += "ldrr R5 SP\n"
        self._push_back()

        nr_of_globals = len([x for x in ir if isinstance(x, GlobalVar)])
        self.current_string += "ldr SP\n"
        self.current_string += "ldc {0}\n".format(nr_of_globals)
        self.current_string += "add\n"
        self.current_string += "str SP\n"
        self.current_string += "ldrr MP SP\n"
        self._push_back()
        
        for irnode in ir:
            self._convert(irnode)
        if self.current_string != "":
            self.current_string += "nop\n"
            self._push_back()
        
        self.byte_code.append(SsmSplBuiltins.HEAD)
        self.byte_code.append(SsmSplBuiltins.ISEMPTY)
        self.byte_code.append(SsmSplBuiltins.TAIL) 
        self.byte_code.append(SsmSplBuiltins.PRINT)
        
        return self.byte_code
            
    def _push_back(self):
        self.byte_code.append(self.current_string)
        self.current_string = ""
    
    def _loadVar(self, varname):
        if varname in self._local_dict.keys():
            return "ldl {0}\n".format(self._local_dict[varname].offset)
        elif varname in self._global_dict.keys():
            tempstr = ""
            tempstr += "ldr R5\n"
            tempstr += "lda {0}\n".format(self._global_dict[varname].offset + 1)
            return tempstr
        else:
            print "This should not happen because of semantic analysis"
            assert(False)

    def _storeVar(self, varname):
        if varname in self._local_dict.keys():
            return "stl {0}\n".format(self._local_dict[varname].offset)
        elif varname in self._global_dict.keys():
            tempstr = ""
            tempstr += "ldr R5\n"
            tempstr += "sta {0}\n".format(self._global_dict[varname].offset + 1)
            return tempstr
        else:
            print "This should not happen because of semantic analysis"
            assert(False)

    def error(self, irnode):
        print "You forgot to implement convert for {0}".format(irnode)
        assert(False)

    def _convert(self, irnode):
        #add label in front of this line
        if   isinstance(irnode, Label):
            self.current_string = str(irnode) + " "
        #process meta GlobalVar instruction (note it down in the global var map)
        elif isinstance(irnode, GlobalVar):
            self._global_dict[irnode.name] = irnode.location
        #process meta LocalVar instruction
        elif isinstance(irnode, LocalVar):
            self._local_dict[irnode.name] = irnode.location
        #process meta Argument instruction
        elif isinstance(irnode, Argument):
            self._local_dict[irnode.name] = irnode.location
            self.current_function_nr_args += 1
        #process meta Function instruction by clearing the function context
        elif isinstance(irnode, FunDecl):
            if self.current_string != "":
                self.current_string += "nop\n"
                self._push_back()
            self._local_dict = {}
            self.current_function_nr_args = 0
        elif isinstance(irnode, Push):
            #ignore this instruction temporarys are stack variables.
            pass
        elif isinstance(irnode, Unop):
            if irnode.operator == "!":
                self.current_string += "not\n"
            elif irnode.operator == "-":
                self.current_string += "neg\n"
            else:
                self.error(irnode)
        elif isinstance(irnode, Binop):
            #All binary operations get handled here (even the booleans)
            if irnode.operator == "*":
                self.current_string += "mul\n"
            elif irnode.operator == "/":
                self.current_string += "div\n"
            elif irnode.operator == "==":
                self.current_string += "eq\n"
            elif irnode.operator == "!=":
                self.current_string += "ne\n"
            elif irnode.operator == ">":
                self.current_string += "gt\n"
            elif irnode.operator == "<":
                self.current_string += "lt\n"
            elif irnode.operator == "<=":
                self.current_string += "le\n"
            elif irnode.operator == ">=":
                self.current_string += "ge\n"
            elif irnode.operator == "-":
                self.current_string += "sub\n"
            elif irnode.operator == "+":
                self.current_string += "add\n"
            elif irnode.operator == ":":
                self.current_string += "stmh 2\n"
            elif irnode.operator == "%":
                self.current_string += "mod\n"
            else:
                self.error(irnode)
            self._push_back()
            
        elif isinstance(irnode, StoreTuple):
            self.current_string += "stmh 2\n"
            self._push_back()
                
        #process all the moves)
        elif isinstance(irnode, Move):
            #Moves to Temp
            if isinstance(irnode.dest, Temp):
                #Copy Const to Temp (load const to stack)
                if isinstance(irnode.source, Const):
                    value = irnode.source
                    if str(value) == "False":
                        value = 0
                    elif str(value) == "True":
                        value = -1
                    else:
                        pass
                    self.current_string += "ldc {0}\n".format(value)
                #Copy Var to Temp (load var onto stack)
                elif isinstance(irnode.source, Ref):
                    self.current_string += self._loadVar(irnode.source.var) 
                #Copy returnTemp Temp (load RR onto stack)
                elif isinstance(irnode.source, ReturnTemp):
                    self.current_string += "ldr RR\n"
                elif isinstance(irnode.source, EmptyList):
                    self.current_string += "ldc 0\n"
                else:
                    self.error(irnode)
                self._push_back()
            
            #Moves to ReturnTemp
            elif isinstance(irnode.dest, ReturnTemp):
                #from Const to ReturnTemp (load top of stack in RR)
                if isinstance(irnode.source, Const):
                    self.current_string += "ldc {0}\n".format(irnode.source)
                #from Var to ReturnTemp (load var in RR)
                elif isinstance(irnode.source, Ref):
                    self.current_string += self._loadVar(irnode.source.var) 
                #from Temp to ReturnTemp (store top of stack in RR)
                elif isinstance(irnode.source, Temp):
                    #only do the else part
                    pass
                else:
                    self.error(irnode)
                self.current_string += "str RR\n"
                self._push_back()
            #Moves to Vars
            elif isinstance(irnode.dest, Ref):
                #from Const to Var (load const to locals)
                if isinstance(irnode.source, Const):
                    self.current_string += "ldc {0}\n".format(irnode.source)
                #from ReturnTemp to Var (load RR on stack and then store it in var)
                elif isinstance(irnode.source, ReturnTemp):
                    self.current_string += "ldr RR\n"
                #from Temp to Var (store top of stack in var)
                elif isinstance(irnode.source, Temp):
                    #only do the else part
                    pass
                else:
                    self.error(irnode)
                self.current_string += self._storeVar(irnode.dest.var)
                self._push_back()
            else:        
                self.error(irnode)
        #Reserve locals.
        #load the SP and nr of locals constant
        #add and store it back in the SP
        elif isinstance(irnode, ReserveLocals):
            self.current_string += "ldr SP\n"
            self.current_string += "ldc {0}\n".format(irnode.nr)
            self.current_string += "add\n"
            self.current_string += "str SP\n"
            self._push_back()
        #CALL
        #Store "old" MP and load the function andress
        #set MP to SP and jump to subroutine.
        
        #SPECIAL-CASE
        #If we are processing main append code to print main's output
        elif isinstance(irnode, Call):
            self.current_string += "ldr MP\n"
            label = str(irnode.label)[0:-1]
            if   label == "fst":
                label = "head"
            elif label == "snd":
                label = "tail"
            else:
                pass
            self.current_string += "ldc {0}\n".format(label)
            self.current_string += "ldrr MP SP\n"
            self.current_string += "jsr\n"
            if (str(irnode.label) == "main:"):
                self.current_string += "ldr RR\n"
                self.current_string += "trap 0\n"
                self.current_string += "halt\n"
            self._push_back()
        #Return
        #set SP to MP and load MP-1 (the old MP) to stack
        #store this value to MP and actually return.
        elif isinstance(irnode, Return):
            self.current_string += "ldrr SP MP\n"
            self.current_string += "ldl {0}\n".format(-1)
            self.current_string += "str MP\n"
            self.current_string += "sts {0}\n".format(-(1+self.current_function_nr_args))
            self.current_string += "ldr SP\n"
            self.current_string += "ldc {0}\n".format(self.current_function_nr_args)
            self.current_string += "sub\n"
            self.current_string += "str SP\n"
            self.current_string += "ret\n"
            self._push_back()
        elif isinstance(irnode, JumpT):
            self.current_string += "brt {0}\n".format(str(irnode.location)[0:-1])
            self._push_back()
        elif isinstance(irnode, JumpF):
            self.current_string += "brf {0}\n".format(str(irnode.location)[0:-1])
            self._push_back()
        elif isinstance(irnode, Jump):
            self.current_string += "bra {0}\n".format(str(irnode.location)[0:-1])
            self._push_back()
        else:
            self.error(irnode)