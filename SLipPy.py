#!/usr/bin/python

import sys
from os.path import join, dirname, abspath, basename, splitext
from inspect import getfile, currentframe

sys.path.append(join(dirname(abspath(getfile(currentframe()))), 'contrib/'))

from colorama import init
init()

from Tokenizer import Tokenizer
from slparser import SLParser   
from prettyPrinter import PrettyPrinter
from semanticanalysis import SemanticAnalysis
from testmanager import TestManager
from intermediate_representation import IntermediateRepresentation

#the backends
from ssmbackend import SSMBackend

from optparse import OptionParser

parser = OptionParser()

parser.add_option("-f", "--file", dest="input", 
                  help="Input SL file to be compiled")
parser.add_option("-o", "--original", action="store_true", dest="print_input", default=False,
                  help="Print the input filename and file content")
parser.add_option("-t", "--tokens", action="store_true", dest="print_tokens", default=False,
                  help="Print the list of token generated")
parser.add_option("-p", "--parse_tree", action="store_true", dest="print_parse_tree", default=False,
                  help="Print the parse tree of the input file")
parser.add_option("-r", "--pretty_print", action="store_true", dest="print_pretty_print", default=False,
                  help="Print the pretty printed program")
parser.add_option("-i", "--intermediate", action="store_true", dest="print_intermediate", default=False,
                  help="print the program in intermediate language")
parser.add_option("--test", action="store_true", dest="test", default=False,
                  help="Run the examples specified in testprograms/* and veries the output.")
parser.add_option("--backend", dest="backend",
                  help="The backend to generate code for (ssm)")

(options, args) = parser.parse_args()

if(options.test):
    TestManager().run()
    exit(0);

if options.input:
    filename = options.input
else:
    print "Required argument: -f/--file <input> not given for more info type SLipPy --help"
    exit(-1)
    
if options.print_input:
    print "\n" + "The input file: \"" + filename + "\"\n"
    f = open(filename, 'r')
    print f.read()
    print "\n"

tokenizer = Tokenizer(filename)

if options.print_tokens:
    print "\nTokens:\n"
    temp_tokenizer = Tokenizer(filename)
    for token in temp_tokenizer.tokenize():
        print token
        
parser = SLParser(tokenizer)
parse_tree = parser.parseProg()

if options.print_parse_tree:
    print "\nParse tree:\n"
    print parse_tree
    
if options.print_pretty_print:
    print "\nPretty printed:\n"
    pprinter = PrettyPrinter()
    pprinter.pprint(parse_tree)
    
if not SemanticAnalysis().checkProg(parse_tree):
    print "Semantic Analysis failed aborting compilation"
    exit(-1)

if options.print_intermediate:
    print "Intermediate Representation\n"
    ir = IntermediateRepresentation().convert(parse_tree)
    for r in ir:
        print r
    print "\n"
        
if options.backend == "ssm":
    ir = IntermediateRepresentation().convert(parse_tree)
    bytecode = SSMBackend().process(ir)
    outputfile = splitext(basename(filename))[0] + ".ssm"
    print "Outputting file {0}".format(outputfile)
    #print "SSM bytecode\n"
    f = open(outputfile, "w")
    for b in bytecode:
        f.write(b)
    print
else:
    print "Unrecognized backend selected"