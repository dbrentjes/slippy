class Type:
    INT = 0
    BOOL = 1
    VOID = 2
    TUPLE = 3
    LIST = 4
    FUNCTION = 5
    GENERIC = 6
    
    TYPES = ["Int", "Bool", "Void", "Tuple", "List", "Function", "Generic"]
    
    def __init__(self, type):
        self.type = type

    def unify(self, other, context):
        if self == other:
            return True
        elif isinstance(self, Type) and isinstance(other, Type) and (self.type == Type.GENERIC or other.type == Type.GENERIC):
            if self.type == Type.GENERIC:
                context.substitute(self, other)
                return True
            elif other.type == Type.GENERIC:
                context.substitute(other, self)
                return True
        elif isinstance(self, TupleType) and isinstance(other, TupleType):
            return self.left.unify(other.left, context) and self.right.unify(other.right, context)
        elif isinstance(self, ListType) and isinstance(other, ListType):
            return self.valueType.unify(other.valueType, context)
        else:
            return False
            
    def __eq__(self, other):
        if isinstance(other, self.__class__) and self.type == other.type:
            if self.type == Type.INT or self.type == Type.BOOL:
                return True
            elif self.type == Type.LIST:
                return self.valueType == other.valueType
            elif self.type == Type.TUPLE:
                return self.left == other.left and self.right == other.right
            elif self.type == Type.FUNCTION:
                return self.argumentTypes == other.argumentTypes and self.returnType == other.returnType
            elif self.type == Type.GENERIC:
                return self.id == other.id
            elif self.type == Type.VOID:
                return True
            else:
                raise ValueError("type is not in int, bool, tuple, list, generic or function: " + Type.TYPES[self.type])
            """elif isinstance(self, Type) and isinstance(other, Type) and (self.type == Type.GENERIC or other.type == Type.GENERIC):
                if self.type == Type.GENERIC:
                    self.type = other.type
                    return True
                elif other.type == Type.GENERIC:
                    other.type = self.type
                    return True
                else:
                    return False
            elif isinstance(self, Type) and isinstance(other, Type):
                return self.type == other.type"""
        else:
            return False
        
    def __ne__(self, other):
        return not self.__eq__(other)
    
    def __str__(self):
        return Type.TYPES[self.type]    
    def __repr__(self):
        return self.__str__()
    
    @staticmethod
    def toString(type):
        if isinstance(type, int):
            return Type.TYPES[type]
        else:
            return str(type)
    
    def substitute(self, genericType, actualType):
        # nothign to substitute here
        pass
        
class TupleType(Type):
    def __init__(self, left, right):
        Type.__init__(self, Type.TUPLE)
        self.left = left
        self.right = right
    def __str__(self):
        return "(" + Type.toString(self.left) + ", " + Type.toString(self.right) + ")"
    def __repr__(self):
        return self.__str__()
    def substitute(self, genericType, actualType):
        if self.left == genericType:
            self.left = actualType
        else:
            self.left.substitute(genericType, actualType)
        if self.right == genericType:
            self.right = actualType
        else:
            self.right.substitute(genericType, actualType)

class ListType(Type):
    def __init__(self, valueType):
        Type.__init__(self, Type.LIST)
        self.valueType = valueType
    def __str__(self):
        return "[" + Type.toString(self.valueType) + "]"
    def __repr__(self):
        return self.__str__()
    def substitute(self, genericType, actualType):
        if self.valueType == genericType:
            self.valueType = actualType
        else:
            self.valueType.substitute(genericType, actualType)
        
class FunctionType(Type):
    def __init__(self, argumentTypes, returnType):
        Type.__init__(self, Type.FUNCTION)
        self.argumentTypes = argumentTypes
        self.returnType = returnType
    def __str__(self):
        return " x ".join(map(Type.toString, self.argumentTypes)) + " -> " + Type.toString(self.returnType)
    def __repr__(self):
        return self.__str__()
    
    def substitute(self, genericType, actualType):
        self.argumentTypes = [actualType if type == genericType else type for type in self.argumentTypes]
        for type in self.argumentTypes:
            type.substitute(genericType, actualType)
            
        if self.returnType == genericType:
            self.returnType = actualType
        else:
            self.returnType.substitute(genericType, actualType)
            
class GenericType(Type):
    def __init__(self, id):
        Type.__init__(self, Type.GENERIC)
        self.id = id
    def __str__(self):
        if self.type != Type.GENERIC:
            return "<{0}> (Deduced to be {1})".format(self.id, Type.TYPES[self.type])
        else:
            return "<" + self.id + ">"
    def __repr__(self):
        return self.__str__()
    
    def substitute(self, genericType, actualType):
        # nothing to substitute here
        pass