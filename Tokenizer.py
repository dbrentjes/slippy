from StringIO import StringIO
from functools import partial
from colorama import Fore

class Tokenizer:
    
    class Token:
        
        def Enum(*sequential, **named):
            enums = dict(zip(sequential, range(len(sequential))), **named)
            reverse = dict((value, key) for key, value in enums.iteritems())
            enums['reverse_mapping'] = reverse
            return type('Enum', (), enums)

        _braces         = ['(', ')', '[', ']', '{', '}']
        _Op1_chars      = ['!']
        _Op2_chars      = ['*', '/', '%']
        _Op3_chars      = ['+']
        _Op4_chars      = ['==', '<', '>', '<=', '>=', '!=']
        _operator_chars = ['+', '-', '*', '/', '%', '=', '<', '>', '!', '|', '&', ',', ':', ';']
        _statements = ['if', 'else', 'while', 'return']
        _bool_constants = ['True', 'False']
        _typeids = ['Void', 'Int', 'Bool']
        _line_comment = '//'
        _block_comment_start = '/*'
        _block_comment_end = '*/'
        
        _Group       = Enum(
                            'id',
                            'int',
                            'operator',
                            'statement',
                            'typeid',
                            'brace',
                            'bool_constant',
                            'line_comment',
                            'block_comment',
                            'unknown',
                        )
        
        Type        = Enum(
                            'id',
                            'int',
                            'linecomment',
                            'blockcomment',
                            
                            ';',
                            
                            'Void',
                            'Int',
                            'Bool',
                            
                            '(',
                            ')',
                            
                            '[',
                            ']',
                            
                            '{',
                            '}',
                            
                            ',',
                            
                            'if',
                            'else',
                            'while',
                            'return',
                            
                            'boolConst',
                            
                            'Op1',
                            'Op2',
                            'Op3',
                            'Op4',
                            
                            ':',
                            '-',
                            '=',
                            
                            '!',
                            
                            '&&',
                            '||',
                            
                            'invalid',
                        )
        
        #Constructs a Token
        def __init__(self, line, column, type, id_name):
            self.line = line
            self.column = column
            self.type = type
            self.id_name = id_name
        
        def __str__(self):
            output = ""
            output += Fore.GREEN + str(Tokenizer.Token.Type.reverse_mapping[self.type]) + Fore.RESET
            if self.type == self.Type.id or self.type == self.Type.Int:
                output += " \"" + self.id_name + "\""
            elif self.id_name in self._typeids:
                output += " \"TypeId\""
            elif all([x in self._operator_chars for x in self.id_name]):
                output += " \"Operator\""
            if self.type == self.Type.invalid:
                output += " " + self.id_name + " "
                
            output += " on line: " + str(self.line) + ":" + str(self.column)
            return output
        
        def isType(self, type):
            return self.type == Tokenizer.Token.Type.__dict__[type]
        
        def isAnyType(self, typeList):
            return any([self.isType(type) for type in typeList])
            
    #Takes a string and initializes a memory file.
    def __init__(self, filename):
        self._column = 1
        self._line = 1
        self._file = open(filename, 'r')
        self._current_token = ""
        self._current_group = None
        self._current_start_line = 0
        self._current_start_column = 0
    
    #takes a char and catogorizes it into what it might be
    def _groups_of_char(self, char):
        Group = Tokenizer.Token._Group
        
        if char in Tokenizer.Token._operator_chars:
            return [Group.operator]
        elif char.isdigit():
            return [Group.int,
                    Group.id]
        elif char.isalpha() or char == '_':
            return [Group.id,
                    Group.statement,
                    Group.typeid]
        else:
            return [Group.unknown]
        
    #given a abstract group and the current actually read token return a Token Type that can be used in the Token constructor.
    def _to_token_type(self, group, token):
        Token = Tokenizer.Token
        Group = Tokenizer.Token._Group        
        Type = Tokenizer.Token.Type
        
        if group == Group.int:
            return Type.int
        if group == Group.id:
            return Type.id
        if group == Group.bool_constant:
            return Type.boolConst
        if group == Group.line_comment:
            return Type.linecomment
        if group == Group.block_comment:
            return Type.blockcomment
        if group == Group.operator:
            if token == '-':
                return Token.Type.__dict__['-']
            if token == ':':
                return Token.Type.__dict__[':']
            if token in Token._Op1_chars:
                return Type.Op1
            if token in Token._Op2_chars:
                return Type.Op2
            if token in Token._Op3_chars:
                return Type.Op3
            if token in Token._Op4_chars:
                return Type.Op4
        return Type.__dict__.get(token, Type.invalid)
            
    def _create_token(self):
        Type = Tokenizer.Token.Type
        Group = Tokenizer.Token._Group
        if self._current_group == Group.id:
            if self._current_token in Tokenizer.Token._typeids:         #if our identifier is actually a typeid
                self._current_group = Group.typeid
            elif self._current_token in Tokenizer.Token._statements:    #if our identifier is actually a statement
                self._current_group = Group.statement
            elif self._current_token in Tokenizer.Token._bool_constants:
                self._current_group = Group.bool_constant
        
        #actual identifier.
        token_line   = self._current_start_line
        token_column = self._current_start_column
        token_type   = self._to_token_type(self._current_group, self._current_token)
        token        = self._current_token
        
        self._current_group = None
        self._current_token = ""
        self._current_start_column = 0
        self._current_start_line = 0
        
        return Tokenizer.Token(token_line, token_column, token_type, token)
    
    def _determine_next_lexeme(self, char):
        Type = Tokenizer.Token.Type
        Group = Tokenizer.Token._Group
        
        if char in Tokenizer.Token._operator_chars:  #Is our new token an operator
            self._current_group = Group.operator
                                
        if char in Tokenizer.Token._braces:
            self._current_group = Group.brace
        
        elif char.isalpha() or char == '_':         #is our new token an id
            self._current_group = Group.id
            
        elif char.isdigit():                        #is our new token an integer
            self._current_group = Group.int
        
        self._current_start_line = self._line         #save the real offset of our curren token instead of using its end.
        self._current_start_column = self._column
        self._current_token = char
    
    #TODO: needs cleanup!!!
    #Generator function that lazily evaluates Tokens
    def tokenize(self):
        Type = Tokenizer.Token.Type
        Group = Tokenizer.Token._Group
        
        for line in self._file: 
            for char in line:
                if self._current_group == Group.line_comment:
                    self._current_token += char
                elif self._current_group == Group.block_comment:
                    self._current_token += char
                    if self._current_token[-2:] == Tokenizer.Token._block_comment_end:
                        self._create_token() #INTENTIONAL NON-YIELD. Comments are tokenized but ignored here.
                elif not char.isspace():
                    groups = self._groups_of_char(char)    #what groups could this char belong to.
                    if groups:                             #is 'char' a valid char?
                        if self._current_group in groups:  #if this 'char' has a group matching with the current group
                            self._current_token += char    #add this 'char' to the current token and check for comments
                            if self._current_token[-2:] == Tokenizer.Token._line_comment:
                                new_group = Group.line_comment
                            elif self._current_token[-2:] == Tokenizer.Token._block_comment_start:
                                new_group = Group.block_comment
                            if self._current_token[-2:] == Tokenizer.Token._line_comment or \
                               self._current_token[-2:] == Tokenizer.Token._block_comment_start:
                                new_token = self._current_token[-2:]
                                self._current_token = self._current_token[0:-2]
                                if len(self._current_token) > 0: #only when a real token preceded the comments yield the token 
                                    yield self._create_token()         
                                self._current_start_line = self._line         #save the real offset of our curren token instead of using its end.
                                self._current_start_column = self._column - len(self._current_token)
                                self._current_group = new_group
                                self._current_token = new_token
                                
                        else:
                            if self._current_group != None:        #if we had a valid group yield a token first. Remember that 0 is false but is a valid enum value.
                                yield self._create_token()
                             
                            self._determine_next_lexeme(char)
                            
                            if self._current_group == Group.brace:
                                yield self._create_token()
                    else:
                        yield Tokenizer.Token(self._current_start_line, self._current_start_column, Type.invalid, self._current_token)
                        
                else:
                    if self._current_group != None:
                        yield self._create_token()
                
                self._column += 1
            if self._current_group == Group.line_comment:
                self._create_token() #INTENTIONAL NON-YIELD. Comments are tokenized but ignored here.
                
            self._line += 1
            self._column = 1
        if self._current_group != None: # return last token till end of stream.
            yield self._create_token()
