\documentclass[a4paper]{article}

\usepackage[english]{babel}
\usepackage{hyperref}
\usepackage{listings}
\usepackage{bussproofs}
\usepackage{amssymb}
\title{SPL Compiler in Python}

\author{Dennis Brentjes (s0815489, \href{mailto:D.Brentjes@student.science.ru.nl}{D.Brentjes@student.science.ru.nl}) \and Manu Drijvers (s3040429, \href{mailto:ManuDrijvers@student.ru.nl}{ManuDrijvers@student.ru.nl})}

\date{\today}

\begin{document}
\maketitle

\section{Implementation language}
The language chosen for implementation is Python 2.7.3. Python is interesting because it is an imperative language with many functional aspects. This gives us the freedom with respect to control flow and declarations of imperative languages, and the benefits of higher order functions. We have some experience programming in Python, but another reason to choose Python is the fact that we would like to learn more about this language.

\section{SPL Grammar}
\begin{lstlisting}
Prog = Decl+

Decl = VarDecl
| FunDecl

VarDecl = Type id '=' Exp ';'

FunDecl = RetType id '(' [ FArgs ] ')' '{' VarDecl* Stmt+ '}'

RetType = Type
| 'Void'

Type = 'Int'
| 'Bool'
| '(' Type ',' Type ')'
| '[' Type ']'
| id

FArgs = Type id [ ',' FArgs ]

Stmt = '{' Stmt* '}'
| 'if' '(' Exp ')' Stmt [ 'else' Stmt ]
| 'while' '(' Exp ')' Stmt
| id '=' Exp ';'
| FunCall ';'
| 'return' [ Exp ] ';'

Exp1 = FunCall
| id 
| Op1 Exp1
| int
| bool
| '(' Exp ')'
| '[]'
| '(' Exp ',' Exp ')'

Exp2 = Exp1 Exp2'*
Exp2' = Op2 Exp1

Exp3 = Exp2 Exp3'*
Exp3' = Op3 Exp2

Exp4 = Exp3 Exp4'*
Exp4' = Op4 Exp3

Exp5 = Exp4 Exp5'*
Exp5' = '&&' Exp4

Exp6 = Exp5 Exp6'*
Exp6' = '||' Exp5

Exp = Exp6 Exp'
Exp' = ':' Exp6 [Exp']

FunCall = id '(' [ ActArgs ] ')'

ActArgs = Exp [ ',' ActArgs ]

Op1 = '!' | '-'

Op2 = '*' | '/' | '%'

Op3 = '+' | '-'

Op4 = '==' | '<' | '>' | '<=' | '>=' | '!='

int = [ '-' ] digit+

bool = 'True' | 'False'

id = alpha ( '_' | alphaNum)*
\end{lstlisting}

Only minor modifications have been made to the grammar. In order to correctly account for operator precedence, we have split the operators in groups of the same precedence. We added multiple productions for expressions, each using a single operator group. The tail recursion in Exp productions allows left associativity. We also removed the left recursion in Exp. Lists are different from expressions with other operators, because lists are right associative. The grammar for list expressions therefore differs from the other expressions.

\pagebreak

\section{Scoping rules}
We will now informally discuss the scoping rules.   
For one shadowing is allowed, you can redeclare a variable in the current scope that exists in a parent scope.

Furthermore we allow referencing all variables and functions in parent scope. Because we adhere to the grammar rules as close as we do we only have 2 scopes. A global scope which contains all  functions and global variables, and the function scope. In which all the variables declared at the top of the function are stored and it has as parent scope the global scope. Variable and function definitions are first searched in the local scope and if it didn't exist there it searches in the global scope. 

We think it is the most common way to do scoping in such a language.

\section{Typing rules}
The typing rules for the normal types are fairly simple and won't be discussed here. \textit{Int} typed variables will only accept \textit{Int} expressions and similar for \textit{Bool} typed variables.

The typing rules of generic types are far more interesting as they involve a clear design choice. 
We only support generic types in function definitions other than the main function. 
This way we can always still check the types and don't have to infer the types in any of our functions. 
Another decision we made is that the functions should always work on the generic type. 
For instance a function taking a  \textit{list of type t} but executes a \textit{operator +} to the elements produce the sum will not be allowed. 
As in this case as we only have \textit{Int} and \textit{Bool} it is nonsensical to generalize them. 
Maybe if we later support things like fixed/floating point variables this will be reconsidered but for now we disallow more general types when the function definition itself restricts this generalized type.

We also decided to threat the generic type variables that have the same identifier to be of the same type.
This might sound as a logical thing to do but we heard quite some discussion regarding how to handle it. a couple of the examples we will show in the test section will reflect this choice.


\begin{prooftree}
\AxiomC{intconst$\in \mathbb{Z}$}
\UnaryInfC{intconst : Int}
\end{prooftree}

\begin{prooftree}
\AxiomC{boolconst$\in \{True, False\}$}
\UnaryInfC{boolconst : Bool}
\end{prooftree}

\begin{prooftree}
\AxiomC{$e_1$ : $\tau_1$}
\AxiomC{$e_2$ : $\tau_2$}
\AxiomC{$\tau = (\tau_1, \tau_2)$}
\TrinaryInfC{$(e_1, e_2) : \tau$}
\end{prooftree}

\begin{prooftree}
\AxiomC{$e : \tau'$}
\AxiomC{$\tau = [\tau']$}
\BinaryInfC{$[e] : \tau$}
\end{prooftree}

\begin{prooftree}
\AxiomC{$e_1 ... e_n : \tau_1 ... \tau_n$}
\AxiomC{$f : \tau_1 ... \tau_n \rightarrow \tau$}
\BinaryInfC{$f(e_1 ... e_n) : \tau$}
\end{prooftree}

\begin{prooftree}
\AxiomC{$e_1 : Bool$}
\AxiomC{$e_2 : Bool$}
\AxiomC{BoolOp in \{\&\&, $\|$\}}
\TrinaryInfC{$e_1$ BoolOp $e_2 : Bool$}
\end{prooftree}

\begin{prooftree}
\AxiomC{$e : Bool$}
\UnaryInfC{$!e : Bool$}
\end{prooftree}

\begin{prooftree}
\AxiomC{$e_1 : Int$}
\AxiomC{$e_2 : Int$}
\AxiomC{IntOp in \{+, -, \%\}}
\TrinaryInfC{$e_1$ IntOp $e_2 : Int$}
\end{prooftree}

\begin{prooftree}
\AxiomC{$e : \tau$}
\UnaryInfC{$(e) : \tau$}
\end{prooftree}

\begin{prooftree}
\AxiomC{$[] : [\tau]$}
\end{prooftree}

\begin{prooftree}
\AxiomC{$e_1 : \tau$}
\AxiomC{$e_2 : [\tau]$}
\BinaryInfC{$e_1 : e_2 : [\tau]$}
\end{prooftree}

\begin{prooftree}
\AxiomC{$\sigma$ is generic}
\UnaryInfC{$\tau: \sigma$}
\end{prooftree}


\section{Tests}
\subsection{Own tests}
\subsubsection{Shadowing}
\begin{lstlisting}
Int a = 3;
Int f()
{
	Bool a = True;
	boolFunc(a);
	return 0;
}
Int g()
{
	intFunc(a);
	return 0;
}
Bool boolFunc(Bool b)
{
	return b;
}
Int intFunc(Int i)
{
	return i;
}
\end{lstlisting}

This example does not cause any typing errors, and therefore shows that shadowing is allowed. The $a$ variable is globally defined as Int, but locally defined as a Bool.

\subsubsection{Type substitution}
\begin{lstlisting}
t id(t a)
{
	return a;
}

Void main()
{
	Int a = id(1);
	Bool b = id(True);
	return;
}


\end{lstlisting}

This program does not cause errors, showing that generic type t can take multiple forms in a single function declaration. 

\subsubsection{strange but correct function calls and definitions}
\begin{lstlisting}
t id(y x)
{
        return x;
}

Int main()
{
        Int x = id(1);
        Bool x = id(1);
        return 0;
}
\end{lstlisting}


This programs shows that we can deduce the right types even when they will not always match. But errors when they don't match.

\begin{quote}
Types not matching: expected Bool but found Int int on line: 8:14
\end{quote}

\subsubsection{Redefinition}
\begin{lstlisting}
Int x = 5;
Int x = 10;

int main()
{
	return 0;
}
\end{lstlisting}

This shows that you cannot define a variable twice. The errors are as follows.
\begin{quote}
Identifier "x" was multiply defined at 2:7

Note: using earlier definition.
\end{quote}

\subsubsection{function overloading on return type}
\begin{lstlisting}
Int func()
{
	return 1;
}

Bool func()
{
	return True;
}

\end{lstlisting}

we have decided to not include overloading on function return types. As a result this program will report the following error

\begin{quote}
Identifier "func" was multiply defined at 6:6

Note: using earlier definition.
\end{quote}

\subsubsection{function overloading on arguments}
\begin{lstlisting}
Void func(Int x)
{
        return;
}

Void func(Bool x)
{
        return;
}
\end{lstlisting}

This programs shows that we do not allow function overloading on function arguments as well, this is our choice to make it slightly easier to do the assignments a possible extension would be to allow function overloading. The error is as follows.

\begin{quote}
Identifier "func" was multiply defined at 6:6

Note: using earlier definition.
\end{quote}

\subsubsection{Tuple type deduction}
\begin{lstlisting}
Void func((i,b) x)
{
        return;
}

Int main()
{
        func((1,True));
        return 0;
}
\end{lstlisting}

This program showed a bug in our compiler, causing it to not be able to derive tuple types. We managed to solve this problem, so this program now passes semantic analysis. 

\subsubsection{list type deduction}

\begin{lstlisting}
Void func([t] x)
{
        return;
}

Int main()
{
        func(1 : 2 : 3 : []);
        return 0;
}
\end{lstlisting}

This program parses just fine and shows the ability to deduce list types.

\subsubsection{Assigning the empty list}
\begin{lstlisting}
Void func([Int] x)
{
        return;
}

Int main()
{
        [Int] x = [];
        func(x);
        return 0;
}

\end{lstlisting}

This might seem trivial but the empty list is a polymorphic thing and so some type of deduction is taking place. This reminds us not to check this deduction. It parses and passes semantic analysis.

\subsubsection{cons operator type-checking}

\begin{lstlisting}
Void listfunc([t] x)
{
        return;
}

Int main()
{
        [Int] x = 1 : 2 : [];
        listfunc(True : x);
        return 0;
}
\end{lstlisting}

This examples parses and behaves as expected but gives a somewhat cryptic error. But the location is clear and this is very helpful

\begin{quote}
Types not matching: expected \textless z\textgreater~but found Bool boolConst on line: 10:11
\end{quote}

\input{tests}

\section{Workload distribution}
Dennis has implemented the tokenizer, which was tested by Manu. Manu implemented the parser, which was tested by Dennis. The semantic analysis for non-generic types has been done by Manu. The generic types has been done partly by Dennis and finished by Manu. The semantic analysis has been tested by Manu. 


\end{document}