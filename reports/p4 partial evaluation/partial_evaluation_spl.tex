\section{Partial Evaluation of Simple Programming Language}
In this section we describe a way to specialize programs in Simple Programming Language (SPL), using a form of off-line partial evaluation. The grammar of this language is defined in appendix \ref{sec:grammar}.

\subsection{Specialized Main}
We will specialize the Main function. We assume that the main function takes $x_1, \ldots, x_n$ input variables. By giving partial input, we create a specialized version of the program. For example, by fixing $x_1, \ldots, x_k$ to values $c_1, \ldots, c_k$ respectively, we define function $main[x_1 \mapsto c_1, \ldots, x_k \mapsto c_k]$ that takes input arguments $x_k, \ldots, x_n$. This specialized main will have the same semantics as the original function, i.e. $main(c_1, \ldots, c_n)$ gives the same results as $main[x_1 \mapsto c_1, \ldots, x_k \mapsto c_k](c_{k+1}, \ldots, c_n)$.

\subsection{Side Effects}
Partial evaluation often focuses on functional languages, because the side-effect freeness simplifies partial evaluation. If a language is free of side-effects, replacing a function call with its return value does not change the behavior of a program. Since SPL does allow side-effects, such as changing global variables or printing values, we define a predicate $SEFunc$ that checks whether a function contains side-effects. This predicate uses predicates $SEExp$ and $SEStmt$, that check whether an expression and statement respectively have side-effects. Using these predicates, we know which function calls we can replace with its evaluation, and which we have to keep in order to keep the same semantics.

\subsubsection{Expressions}
Expressions can have side-effects by calling a function that has side effects.

\begin{enumerate}
	\item $SEExp(FunCall) := SEFunc(f)$
	\item $SEExp(\_) := false$
\end{enumerate}

\subsubsection{Statements}
Statements can have side-effects by assigning to global variables, calling functions that have side-effects or by using expressions with side-effects.

\begin{enumerate}
	\item $SEStmt({Stmt^*}) := \bigvee_{stmt \in Stmt^*}SEStmt(stmt)$
	\item $SEStmt(IfStmt) := SEExp(conditional) \lor SEStmt(thenStmt) \lor SEStmt(elseStmt)$
	\item $SEStmt(WhileStmt) := SEExp(conditional) \lor SEStmt(body)$
	\item $SEStmt(id = exp) := SEExp(exp) \lor id\ is\ global$
	\item $SEStmt(FunCall) := SEFunc(f)$
	\item $SEStmt(Return) := SEExp(exp)$
\end{enumerate}

\subsubsection{Functions}
A function has side-effects if any of its statements have side-effects, or when it is the $Print$ function.

\begin{enumerate}
	\item $SEFunc(Print) := true$
	\item $SEFunc(f) := \bigvee_{stmt \in Stmt^*}SEStmt(stmt)$
\end{enumerate}

\subsection{Static and Dynamic variables}
As shown in figure \ref{fig:compilationsteps}, the partial evaluator receives a some partial input. We call the list of input variables of the main function $Input$. The variables that are specified by the partial input are called $Static$, and the other variables are called $Dynamic$, which is equal to $Input \setminus Static$.

\subsection{Binding-Time Analysis}
The binding time analyzer will place annotations on the AST. It will `underline' which part of the program can be precomputed compile time. We start in the $main$ function, for every expression and statement we check whether it can be precomputed, and if so, we underline it.

\subsubsection{Expressions}
An expressions should be underlined when its value is fixed by the partial input $Input$. $Exp_n$ denotes an expression of type $Exp, Exp6, Exp5, Exp4, Exp3, Exp2$. Expression $e$ will be underlined if and only if $UExp(e) = true$.

\begin{enumerate}
	\item $UExp(FunCall) := \neg SEFunc(f) \land \bigwedge_{arg \in ActArgs} UExp(arg)$
	\item $UExp(int) := true$
	\item $UExp(bool) := true$
	\item $UExp(id) := id \in Static$
	\item $UExp( (Exp) ) := UExp(Exp)$
	\item $UExp([]) := true$
	\item $UExp((FstExp, SndExp)) := UExp(FstExp) \land UExp(SndExp)$
	\item $UExp(Exp_n) := UExp(Exp_{n-1}) \land \bigwedge_{Exp_n' \in Exp_n'^*} UExp(Exp_n')$
\end{enumerate}

\subsubsection{Statements}
A statement can be evaluated when it contains a conditional that is constant with partial input $Static$, or when it calls a function without storing its result that has no side-effects. Statement $s$ will be underlined if and only if $UStmt(s) = true$.

\begin{enumerate}
	\item $UStmt(IfStmt) := UExp(conditional)$
	\item $UStmt(WhileStmt) := UExp(conditional)$
	\item $UStmt(FunCall) := \neg SEFunc(f)$
	\item $UStmt(\_) := false$
\end{enumerate}

\subsection{Specializer}
After the binding-time analysis, the specializer computes the specialized program. This process is fairly simple. Every underlined expression is evaluated, using the partial input $Input$, and the expression is replaced with its result. For an underlined if-statement the conditional is evaluated. If it evaluates to true, we replace the if-statement with the `then' block. If it evaluates to false, we replace the if-statement with the `else' block if present, or simply remove the if-statement if no `else' is defined. For an underlined while-statement, we also evaluate the conditional. If this is false, we eliminate the while-statement. Underlined $FunCall$ statements will also be removed. 


\subsection{Examples}
Lets examine some example programs and how they would be rewritten.
In these exampes the set of static variables at the start wil be \{static\} and the set of dynamic variables will be \{dynamic\}

\noindent\begin{minipage}[t]{.48\textwidth}
\begin{scriptsize}

\lstset{ 
basicstyle=\ttfamily,
moredelim=[is][\underbar]{_}{_}
}

\begin{lstlisting}[caption={\scriptsize Annotated code},frame=tlrb,keepspaces=true]{annotated 1}
Void report(T x)
{
  print(0);
  print(x);
  print(0);
}

Int main(Int static, Int dynamic)
{
  if(_static < 10_)
  {
    if(dynamic > 0)
    {
      report(dynamic);
    }
  }
  else
  {
    report(dynamic);
  }
  report(_static_);
  return 0;
}
\end{lstlisting}
\end{scriptsize}
\end{minipage}\hfill
\begin{minipage}[t]{.48\textwidth}
\begin{scriptsize}

\lstset{
	basicstyle=\ttfamily,
}
	
\begin{lstlisting}[caption={\scriptsize Specialized code for static = 5},frame=tlrb]{specialized 1}
Void report(T x)
{
  print(0);
  print(x);
  print(0);
}

Int _main_5(Int dynamic)
{
  if(dynamic > 0)
  {
    report(dynamic);
  }
  report(5);
  return 0;
}
\end{lstlisting}
\end{scriptsize}
\end{minipage}

This example shows an side effect heavy calculation, but by fixing just 1 of the input variables one branch check and one branch contents is completely removed even though this branch contained code with side-effects.
Note that the annotator does not need to know for what we specialize our code it just knows it can specialize on the if and later it will be decided if the else or then branch must be kept.
In the simple stack machine (which has no pipeline and there fore no branch prediction) this is not really a big improvement, but eliminating a branch in languages such as C or C\texttt{++} running on a modern machine can make a big difference in performance

\noindent\begin{minipage}[t]{.48\textwidth}
\begin{scriptsize}

\lstset{
	basicstyle=\ttfamily,
	moredelim=[is][\underbar]{_}{_}
}

\begin{lstlisting}[caption={\scriptsize Annotated code},frame=tlrb,keepspaces=true]{annotated 2}
Int main(Int static, Int dynamic)
{
  Int y = _static * -1 + 8_
  while(_y > 4_)
  {
    print(y);
    y = y - 1;
  }
  return y;
}
\end{lstlisting}
\end{scriptsize}
\end{minipage}\hfill
\begin{minipage}[t]{.48\textwidth}
\begin{scriptsize}

\lstset{ 
	basicstyle=\ttfamily,
}

\begin{lstlisting}[caption={\scriptsize Specialized code for static = 3},frame=tlrb]{specialized 2}
Int _main_3(Int dynamic)
{
  Int y = 5;
  while(y > 4)
  {
    print(y);
    y = y - 1;
  }
  return y;
}
\end{lstlisting}
\end{scriptsize}
\end{minipage}

After executing the first specialization step by folding the the expression left of the $Int\ y$, which is completely static and side-effect free the $Int\ y$ becomes part of the static input of the program.
This allows the expression in the while condition to be evaluated.
Note that $y = y - 1$ is not annotated in the source as we do not touch the while bodies. 
As analyzing the loop might take a large amount of time or even not terminate at all. 
In this case it analyzes if it will enter the loop if yes we leave it in else we throw away the loop.









