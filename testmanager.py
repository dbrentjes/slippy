
import os, sys

from Tokenizer import Tokenizer
from slparser import SLParser   
from prettyPrinter import PrettyPrinter
from semanticanalysis import SemanticAnalysis

from tempfile import TemporaryFile

class TestManager:
    
    def __init__(self):
        pass
        
    def run(self):        
        test_path = "testprograms"
        result_path = "testresults"
        filenames = os.listdir(test_path)
        #filenames = [os.path.join(test_path, x) for x in filenames]
        for entry in filenames:
            path = os.path.join(test_path, entry)
            if os.path.isfile(path):
                print "testing: {0}".format(path)
                tokenizer = Tokenizer(path)
                parser = SLParser(tokenizer)
                parse_tree = parser.parseProg()

                old_stdout = sys.stdout
                log_file = TemporaryFile()
                sys.stdout = log_file
                
                SemanticAnalysis().checkProg(parse_tree)
                
                sys.stdout = old_stdout
                log_file.seek(0)
                
                result_file = os.path.join(result_path, os.path.splitext(entry)[0] + ".res")
                
                if os.path.isfile(result_file):
                    if open(result_file).read() == log_file.read():
                        print "Test Passed:"
                        continue
                    else:
                        query = "The expected result and current result differ: is this acceptable? (Y/n)"
                    log_file.seek(0)
                else:
                    query = "There was no reference result available, save this run? (Y/n)"
                    
                print "Program:"
                
                
                PrettyPrinter().pprint(parse_tree)

                print ''
                
                print "output:"
                print log_file.read();
                log_file.seek(0)
                print ''
                
                yes_input = ['', 'y', 'Y']
                no_input = ['n', 'N']
                acceptable_input = yes_input + no_input
                input = raw_input(query)
                while input not in acceptable_input:
                    input = raw_input(query)
                    
                if input in yes_input:
                    open(result_file, 'w').write(log_file.read())
                    
                print "\n\n"