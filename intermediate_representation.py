
from irnodes import *
import re

class IntermediateRepresentation:
    
    def __init__(self):
        self.representation = []
        
    def convert(self, ast):
        assert ast.type == "Prog"
        for node in ast.children:
            if node.type == "VarDecl":
                self._addGlobalVarDecl(node)
            
        self.representation.append(Call(Label("main")))
            
        for node in ast.children:
            if node.type == "FuncDecl":
                self._addFunDecl(node)
                
        return self.representation
                
    def _addGlobalVarDecl(self, node):
        var = GlobalVar(node.children[1].children[0].type)
        self.representation.append(var)
        temporary = Temp()
        self._store(temporary, node.children[2])
        self.representation.append(Move(Ref(var.name), temporary)) 
        free_top_temporary()
        
    def _addFunDecl(self, node):
        self.representation.append(FunDecl(node.children[1].children[0].type))
        fun_label = Label(node.children[1].children[0].type)
        self.representation.append(fun_label)
        
        fargs_list = self._fargsToList(node.children[2])
        nr_args = len(fargs_list)
        for i in range(nr_args):
            argument = Argument(fargs_list[i][1].children[0].type, Location(MarkPointer(),  -2 -i))
            self.representation.append(argument)
            
        nr_of_vardecls = len(node.children[3].children)
        self.representation.append(ReserveLocals(nr_of_vardecls))
        for i in range(nr_of_vardecls):
            decl = node.children[3].children[i]
            local_var = LocalVar(decl.children[1].children[0].type, Location(MarkPointer(), i+1))
            self.representation.append(local_var)            
            temporary = Temp()
            self._store(temporary, decl.children[2])
            self.representation.append(Move(Ref(local_var.name), temporary))
            free_top_temporary()
        
        for stmt in node.children[4].children:
            self._translate(stmt.children[0])
        
        for _ in range(nr_of_vardecls):# + nr_of_args):
            free_top_temporary

    def _fargsToList(self, node):
        if   len(node.children) == 0:
            return []
        elif len(node.children) == 1:
            return self._fargsToList(node.children[0])
        else:
            return [(node.children[0], node.children[1])] + self._fargsToList(node.children[2])
    
    def _aargsToList(self, node):
        if  len(node.children) == 0:
            return []
        elif len(node.children) == 1:
            return self._aargsToList(node.children[0])
        else:
            return [node.children[0]] + self._aargsToList(node.children[1])

    def get_cons_list(self, node):
        if len(node.children) == 0:
            return []
        elif len(node.children) == 1:
            return [node.children[0]]
        else:
            return self.get_cons_list(node.children[1]) + [node.children[0]]
        
    def _store(self, storage, node):
        cs = node.children
        if   node.type == "int":
            self.representation.append(Move(storage, Const(cs[0].type)))
        elif node.type == "bool":
            self.representation.append(Move(storage, Const(cs[0].type)))
        elif node.type == "ParenthesesExp":
            self._store(storage, cs[0])
        elif node.type == "RetValue":
            self._store(storage, cs[0])
        elif node.type == "id":
            self.representation.append(Move(storage, Ref(cs[0].type)))
        elif node.type == "Op1Exp":
            self._store(storage, cs[1])
            self.representation.append(Unop(cs[0].children[0].type, storage))
        elif re.match("^Exp(\\d)+", node.type):
            self._store(storage, cs[0])
            if len(cs) > 1 and re.match("^Exp\\dPrime+", cs[1].type):
                for expnode in cs[1].children:
                    if len(expnode.children) > 1:
                        temporary = Temp()
                        self._store(temporary, expnode.children[1])
                        self.representation.append(Binop(expnode.children[0].children[0].type, storage, temporary))
                        free_top_temporary()
                    else:
                        self._store(storage, expnode)
            elif node.type == "Exp1" and node.children[0].type == "EmptyList":
                self.representation.append(Move(storage, EmptyList()))
                
        elif node.type == "Exp":
            cons_list = self.get_cons_list(node)
            temporary1 = Temp()
            self._store(temporary1, cons_list[0])
            for i in range(len(cons_list)-1) :
                temporary2 = Temp()
                self._store(temporary2, cons_list[i+1])
                self.representation.append(Binop(":", temporary1, temporary2))
                free_top_temporary()
            free_top_temporary()
        elif node.type == "TupleExp":
            temporary1 = Temp()
            temporary2 = Temp()
            self._store(temporary1, cs[1])
            self._store(temporary2, cs[0])
            self.representation.append(StoreTuple(temporary1, temporary2))
            free_top_temporary()
            free_top_temporary()
        elif node.type == "FunCall":
            self._translate(node)
            self.representation.append(Move(storage, ReturnTemp()))
        elif node.type == "EmptyList":
            Move(storage, EmptyList())
            
        else:
            print "forgot to implement store for: {0}".format(node.type)
            print node
            assert(False)
        
    def _translate_return(self, ret):
        if len(ret.children[0].children) == 1:
            temporary = Temp()
            self._store(temporary, ret.children[0])
            self.representation.append(Move(ReturnTemp(), temporary))
            self.representation.append(Return())
            free_top_temporary()
        else:
            self.representation.append(Return())
        
    def _translate_if(self, ifs):
        tlabel = TrueLabel()
        flabel = FalseLabel()
        temporary = Temp()
        self._store(temporary, ifs.children[0])
        self.representation.append(JumpT(temporary, tlabel))
        free_top_temporary()
        for node in ifs.children[2].children:
            self._translate(node.children[0])
        self.representation.append(Jump(flabel))
        self.representation.append(tlabel)
        for node in ifs.children[1].children:
            self._translate(node)
        self.representation.append(flabel)
    
    def _translate_ass(self, ass):
        temporary = Temp()
        self._store(temporary, ass.children[1])
        self.representation.append(Move(Ref(ass.children[0].children[0].type), temporary) )
        free_top_temporary()
    
    def _translate_while(self, whiles):
        llabel = LoopLabel()
        elabel = EndLabel()
        
        self.representation.append(llabel)
        temporary = Temp()
        self._store(temporary, whiles.children[0])
        self.representation.append(JumpF(temporary, elabel))
        free_top_temporary
        
        self._translate(whiles.children[1])        
        self.representation.append(Jump(llabel))
        self.representation.append(elabel)
        
    def _translate_funcall(self, funcalls):
        args_list = self._aargsToList(funcalls.children[1])
        args_list.reverse()
        for arg in args_list:
            temporary = Temp()
            self._store(temporary, arg)
            self.representation.append(Push(temporary))
        self.representation.append(Call(Label(funcalls.children[0].children[0].type)))
        for arg in args_list:
            free_top_temporary()
    
    def _translate(self, stmt):
        if   stmt.type == "ReturnStmt":
            self._translate_return(stmt)
        elif stmt.type == "IfStmt":
            self._translate_if(stmt)
        elif stmt.type == "Assignment":
            self._translate_ass(stmt)
        elif stmt.type == "WhileStmt":
            self._translate_while(stmt)
        elif stmt.type == "FunCall":
            self._translate_funcall(stmt)
        
        #Simple forwarders    
        elif stmt.type == "Stmt":
            self._translate(stmt.children[0])
        elif stmt.type == "StmtBlock":
            for child in stmt.children:
                self._translate(child)
        
        else:
            print "forgot to implement translate for: {0}".format(stmt.type)
            print stmt
            assert(False)
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        