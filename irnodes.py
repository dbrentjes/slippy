
class Label(object):
    
    def __init__(self, label):
        self.label = str(label)
    
    def __str__ (self):
        return self.label + ":"

class TrueLabel(Label):

    nr = 0
    
    def __init__(self):
        super(TrueLabel, self).__init__("IfTrue{0}".format(TrueLabel.nr))
        TrueLabel.nr += 1

class FalseLabel(Label):
    
    nr = 0
    
    def __init__(self):
        super(FalseLabel, self).__init__("ifFalse{0}".format(FalseLabel.nr))
        FalseLabel.nr += 1

class LoopLabel(Label):
    
    nr = 0
    
    def __init__(self):
        super(LoopLabel, self).__init__("Loop{0}".format(LoopLabel.nr))
        LoopLabel.nr += 1

class EndLabel(Label):
    
    nr = 0
    
    def __init__(self):
        super(EndLabel, self).__init__("End{0}".format(EndLabel.nr))
        EndLabel.nr += 1

class EmptyList:
    
    def __str__():
        return "[]"
        
class StoreTuple:
    
    def __init__(self, fst, snd):
        self.fst = fst
        self.snd = snd
    
    def __str__(self):
        return "StoreTuple {0} {1}".format(self.fst, self.snd)

class JumpT:
    
    def __init__(self, temporary, location):
        self.temp = temporary
        self.location = location
        
    def __str__(self):
        return "JumpT {0} {1}".format(self.temp, self.location)
    
class JumpF:
    def __init__(self, temporary, location):
        self.temp = temporary
        self.location = location
        
    def __str__(self):
        return "JumpF {0} {1}".format(self.temp, self.location)
    
class Call:
    def __init__(self, label):
        self.label = label
        
    def __str__(self):
        return "Call {0}".format(self.label)
    
class Jump:
    
    def __init__(self, location):
        self.location = location
        
    def __str__(self):
        return "Jump {0}".format(self.location)

class Const:
    
    def __init__(self, const):
        self.const = const
        
    def __str__(self):
        return str(self.const)
    
class Ref:
    
    def __init__(self, var):
        self.var = var
        
    def __str__(self):
        return '%' + str(self.var)
    
class Temp:
    
    in_use = 0

    def __init__(self):
        self.nr = Temp.in_use
        Temp.in_use += 1
    
    def __str__(self):
        return "temp" + str(self.nr)
    
def free_top_temporary():
        Temp.in_use -= 1
    
class Move:
    
    def __init__(self, dest, source):
        self.dest = dest
        self.source = source
    
    def __str__(self):
        return "Move " + str(self.dest) + " " + str(self.source)
    
class Binop:

    def __init__(self, operator, dest, source):
        self.operator = operator
        self.dest     = dest
        self.source   = source
    
    def __str__(self):
        return " ".join([str(x) for x in ['('+self.operator+')', self.dest, self.source]])
    
class Unop:

    def __init__(self, operator, operand):
        self.operator = operator
        self.operand = operand

    def __str__(self):
        return " ".join([str(x) for x in [self.operator, self.operand]])

class FunDecl():
    def __init__(self, name):
        self.name = name
        
    def __str__(self):
        return "Function \"{0}\"".format(self.name)

class Var(object):
    
    def __init__(self, name, location):
        self.name = name
        self.location = location
    
    def __str__(self):
        return "{0} at {1}".format(self.name, self.location)

class GlobalVar(Var):
    
    nr_globals = 0
    
    def __init__(self, name):
        super(GlobalVar, self).__init__(name, Location(StartOfStaticData(), GlobalVar.nr_globals))
        GlobalVar.nr_globals += 1
        
    def __str__(self):
        return "global " + Var.__str__(self)
    
class LocalVar(Var):
    
    def __init__(self, name, location):
        super(LocalVar, self).__init__(name, location)
    
    def __str__(self):
        return "local " + Var.__str__(self)

class Argument(Var):
    def __init__(self, name, location):
        super(Argument, self).__init__(name, location)
    
    def __str__(self):
        return "argument " + Var.__str__(self)
    
class Push:
    
    def __init__(self, arg):
        self.arg = arg
        
    def __str__(self):
        return "Push {0}".format(self.arg)
    
class Pop:
    
    def __init__(self, storage):
        self.storage = storage
    
    def __str__(self):
        return "Pop {0}".format(self.storage)
    
class Return:

    def __init__(self, ret_exp = None):
        self.ret_exp = ret_exp
    
    def __str__(self):
        return "Return " + str(self.ret_exp)
    
class ReturnTemp:

    def __init__(self):
        pass
        
    def __str__(self):
        return "rettemp"
    
class Register:
    def __init__(self, nr):
        self.nr = nr
    
    def __str__(self):
        return "Register {0}".format(self.nr)
    
class EmptyList:
    
    def __init__(self):
        pass 
        
    def __str__(self):
        return "[]"
        
class MarkPointer:
    
    def __init__(self):
        pass
        
    def __str__(self):
        return "MP"

class StackPointer:
    def __init__(self):
        pass
        
    def __str__(self):
        return "SP"
    
class Location:
    
    def __init__(self, loc, offset=0):
        self.loc = loc
        self.offset = offset
        
    def __str__(self):
        return "{0} with offset {1}".format(self.loc, self.offset)
    
class StartOfStaticData:
    
    def __init__(self):
        pass
        
    def __str__(self):
        return "SDP"
    
class ReserveLocals:
    
    def __init__(self, nr):
        self.nr = nr
    
    def __str__(self):
        return "Reserve space for {0} locals".format(self.nr)
    