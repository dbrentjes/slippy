class SsmSplBuiltins:

    ISEMPTY =\
"""isEmpty: ldl -2
ldc 0
eq
str RR
ldrr SP MP
ldl -1
str MP
sts -2
ldr SP
ldc 1
sub
str SP
ret
"""
    
    HEAD =\
"""head: ldl -2
ldr MP
ldc isEmpty
ldrr MP SP
jsr
ldr RR
brf hashead
ldc -1
trap 0
halt
hashead: ldl -2
ldh 0
str RR
ldrr SP MP
ldl -1
str MP
sts -2
ldr SP
ldc 1
sub
str SP
ret
"""

    TAIL =\
"""tail: ldl -2
ldr MP
ldc isEmpty
ldrr MP SP
jsr
ldr RR
brf hastail
ldc -2
trap 0
halt
hastail: ldl -2
ldh -1
str RR
ldrr SP MP
ldl -1
str MP
sts -2
ldr SP
ldc 1
sub
str SP
ret
"""

    PRINT=\
"""print: ldl -2
trap 0
ldrr SP MP
ldl -1
str MP
sts -2
ldr SP
ldc 1
sub
str SP
ret
"""


