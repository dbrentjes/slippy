
\documentclass{beamer}

\usepackage[english]{babel}
\usepackage{graphicx,hyperref,url}
\usepackage{ru}

\usepackage{listings}
\usepackage{framed}
\usepackage{bussproofs}
\usepackage{amssymb}
\usepackage{mathptmx}
\usepackage[scaled=0.9]{helvet}
\usepackage{courier}

% The title of the presentation:
%  - first a short version which is visible at the bottom of each slide;
%  - second the full title shown on the title slide;
\title[Partial Evaluation]{Partial Evaluation of SPL}

% Optional: a subtitle to be dispalyed on the title slide
\subtitle{}

% The author(s) of the presentation:
%  - again first a short version to be displayed at the bottom;
%  - next the full list of authors, which may include contact information;
\author{Dennis Brentjes \and Manu Drijvers}

% The institute:
%  - to start the name of the university as displayed on the top of each slide
%    this can be adjusted such that you can also create a Dutch version
%  - next the institute information as displayed on the title slide
\institute[Radboud University Nijmegen]{Radboud University Nijmegen}

% Add a date and possibly the name of the event to the slides
%  - again first a short version to be shown at the bottom of each slide
%  - second the full date and event name for the title slide
\date[21-06-2013]{21-06-2013}

\begin{document}

\maketitle

\begin{frame}
\frametitle{Partial Evaluation}
\begin{itemize}
	\item Program optimization
	\item Given partial input
	\item Create specialized program
	\item Do some of the work compile-time
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Partial Evaluation}
\begin{figure}
\centerline{\includegraphics[scale=0.6]{figure/compiler_steps}}
\end{figure}
\end{frame}


\begin{frame}
\frametitle{Partial Evaluation steps}
\begin{enumerate}
	\item Binding-time analysis
	\begin{itemize}
		\item Receives which input is fixed
		\item Places annotations on the program
		\item Determines which portions should be evaluated
	\end{itemize}
	
	\item Specializer
		\begin{itemize}
		\item Receives annotated program and partial input
		\item Partially evaluates program
		\item Returns specialized program
	\end{itemize}
\end{enumerate}
\end{frame}


\begin{frame}
\frametitle{Partial Evaluation of SPL}
\begin{itemize}
	\item Assume $main$ takes input $x_1, \ldots, x_n$
	\item Specialize $main$ function
	\item Partial input: $Static \subseteq \{x_1, \ldots, x_n\}$
	\item $main(c_1, \ldots, c_n) = main_{x_1 \mapsto c_1, \ldots, x_k \mapsto c_k}(c_{k+1}, \ldots, c_n)$
\end{itemize}
\end{frame}


\begin{frame}
\frametitle{Side-effects}
\begin{itemize}
	\item Eliminating something with side-effects changes behaviour
	\item Define functions to check for side-effects
	\item Do not eliminate segments with side-effects
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Side-effects Expressions}
\begin{enumerate}
	\item $SEExp(FunCall) := SEFunc(f)$
	\item $SEExp(\_) := false$
\end{enumerate}
\end{frame}

\begin{frame}
\frametitle{Side-effects Statements}
\begin{enumerate}
	\item $SEStmt({Stmt^*}) := \bigvee_{stmt \in Stmt^*}SEStmt(stmt)$
	\item $SEStmt(IfStmt) := SEExp(conditional) \lor SEStmt(thenStmt) \lor SEStmt(elseStmt)$
	\item $SEStmt(WhileStmt) := SEExp(conditional) \lor SEStmt(body)$
	\item $SEStmt(id = exp) := SEExp(exp) \lor id\ is\ global$
	\item $SEStmt(FunCall) := SEFunc(f)$
	\item $SEStmt(Return) := SEExp(exp)$
\end{enumerate}
\end{frame}

\begin{frame}
\frametitle{Side-effects Functions}
\begin{enumerate}
	\item $SEFunc(Print) := true$
	\item $SEFunc(f) := \bigvee_{stmt \in Stmt^*}SEStmt(stmt)$
\end{enumerate}
\end{frame}


\begin{frame}
\frametitle{Binding-time Analysis}
\begin{itemize}
	\item Underline statements and expressions that can be precomputed
	\item Use the functions that check for side-effects, do not underline these.
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Binding-time Analysis expressions}
\begin{enumerate}
	\item $UExp(FunCall) := \neg SEFunc(f) \land \bigwedge_{arg \in ActArgs} UExp(arg)$
	\item $UExp(int) := true$
	\item $UExp(bool) := true$
	\item $UExp(id) := id \in Static$
	\item $UExp( (Exp) ) := UExp(Exp)$
	\item $UExp([]) := true$
	\item $UExp((FstExp, SndExp)) := UExp(FstExp) \land UExp(SndExp)$
	\item $UExp(Exp_n) := UExp(Exp_{n-1}) \land \bigwedge_{Exp_n' \in Exp_n'^*} UExp(Exp_n')$
\end{enumerate}
\end{frame}


\begin{frame}
\frametitle{Binding-time Analysis Statements}
\begin{enumerate}
	\item $UStmt(IfStmt) := UExp(conditional)$
	\item $UStmt(WhileStmt) := UExp(conditional)$
	\item $UStmt(FunCall) := \neg SEFunc(f)$
	\item $UStmt(\_) := false$
\end{enumerate}
\end{frame}

\begin{frame}
\frametitle{Specializer}
\begin{itemize}
	\item Evaluate every underlined statement and expression
	\item Replace it for its result in the AST
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Example 1}
\begin{columns}
  \begin{column}[t]{0.5\textwidth}
	\begin{tiny}
	
	\lstset{ 
	  basicstyle=\ttfamily,
	  moredelim=[is][\underbar]{_}{_}
	}
	
	\begin{lstlisting}[caption={\scriptsize Annotated code},frame=tlrb,keepspaces=true]{annotated 1}
	Void report(T x)
	{
	  print(0);
	  print(x);
	  print(0);
	}
	
	Int main(Int static, Int dynamic)
	{
	  if(_static < 10_)
	  {
	    if(dynamic > 0)
	    {
	      report(dynamic);
	    }
	  }
	  else
	  {
	    report(dynamic);
	  }
	  report(_static_);
	  return 0;
	}
	\end{lstlisting}
	\end{tiny}
  \end{column}
  \begin{column}[t]{0.5\textwidth}
	\begin{tiny}
	
	\lstset{
	  basicstyle=\ttfamily,
	}
	
	\begin{lstlisting}[caption={\scriptsize Specialized code for static = 5},frame=tlrb]{specialized 1}
	Void report(T x)
	{
	  print(0);
	  print(x);
	  print(0);
	}
	
	Int _main_5(Int dynamic)
	{
	  if(dynamic > 0)
	  {
	    report(dynamic);
	  }
	  report(5);
	  return 0;
	}
	\end{lstlisting}
	\end{tiny}
  \end{column}
\end{columns}
\end{frame}

\begin{frame}[fragile]
\frametitle{Example 2}

\begin{columns}
  \begin{column}[t]{0.5\textwidth}
	\begin{tiny}
	
	\lstset{
	  basicstyle=\ttfamily,
	  moredelim=[is][\underbar]{_}{_}
	}
	
	\begin{lstlisting}[caption={\scriptsize Annotated code},frame=tlrb,keepspaces=true]{annotated 2}
	Int main(Int static, Int dynamic)
	{
	  Int y = _static * -1 + 8_
	  while(_y > 4_)
	  {
	    print(y);
	    y = y - 1;
	  }
	  return y;
	}
	\end{lstlisting}
	\end{tiny}
  \end{column}
  \begin{column}[t]{0.5\textwidth}
	\begin{tiny}
	
	\lstset{ 
	  basicstyle=\ttfamily,
	}
	
	\begin{lstlisting}[caption={\scriptsize Specialized code for static = 3},frame=tlrb]{specialized 2}
	Int _main_3(Int dynamic)
	{
	  Int y = 5;
	  while(y > 4)
	  {
	    print(y);
	    y = y - 1;
	  }
	  return y;
	}
	\end{lstlisting}
	\end{tiny}
  \end{column}
\end{columns}
\end{frame}

\begin{frame}
\frametitle{Questions?}
\begin{Huge}
\centerline{Questions?}
\end{Huge}
\end{frame}

\end{document}