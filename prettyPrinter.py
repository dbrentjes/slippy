
import re
from colorama import Fore

class PrettyPrinter:

    def __init__(self):
        self._nr_tabs = 0
        
    def _tabs(self):
        return ''.join(["\t"] * self._nr_tabs) 
    
    def _toString(self, node):
        if not node: return ""
        cs = node.children
        temp_str = ""
        
        if node.type == "Prog":
            for child in cs:
                temp_str += self._toString(child)
            return temp_str
        
        elif node.type == "VarDecl":
            return self._tabs() + "{0} {1} {3}={4} {2}".format(self._toString(cs[0]), self._toString(cs[1]), self._toString(cs[2]), Fore.RED, Fore.RESET) + Fore.RED + ";" + Fore.RESET + '\n'

        elif node.type == "FuncDecl":
            temp_str += "{0} {1}({2})\n{{\n".format(self._toString(cs[0]), self._toString(cs[1]), self._toString(cs[2])[:-2]) 
            self._nr_tabs += 1
            temp_str += "{0}{1}".format(self._toString(cs[3]), self._toString(cs[4]))
            self._nr_tabs -= 1
            temp_str += "}\n\n"
            return temp_str
        
        elif node.type == "Void":
            return Fore.BLUE + "Void" + Fore.RESET
        
        elif node.type == "Int":
            return Fore.BLUE + "Int" + Fore.RESET
        
        elif node.type == "Bool":
            return Fore.BLUE + "Bool" + Fore.RESET
        
        elif node.type == "List":
            return "[{0}]".format(self._toString(cs[0]))
        
        elif node.type == "Tuple":
            return "({0}, {1})".format(self._toString(cs[0]), self._toString(cs[1]))
        
        elif node.type == "int":
            return Fore.YELLOW + cs[0].type + Fore.RESET
        
        elif node.type == "bool":
            return Fore.YELLOW + cs[0].type + Fore.RESET
                
        elif node.type == "id":
            return cs[0].type
        
        elif node.type == "EmptyList":
            return Fore.YELLOW + "[]" + Fore.RESET
        
        elif node.type == 'RetType':
            return self._toString(cs[0])
        
        elif node.type == 'FArgs':
            if cs == []:
                return ""
            if len(cs) == 1:
                return self._toString(cs[0])
            else:
                temp_str = "{0} {1}, {2}".format(self._toString(cs[0]), self._toString(cs[1]), self._toString(cs[2])) 
                return temp_str
            
        elif node.type == 'TypeId':
            return Fore.BLUE + self._toString(cs[0]) + Fore.RESET
        
        elif node.type == 'VarDecls':
            for child in cs:
                temp_str += self._toString(child)
            return temp_str
        
        elif node.type == 'Stmts':
            for child in cs:
                temp_str += self._toString(child)
            return temp_str
        
        elif node.type == 'Stmt':
            temp_str = self._tabs() + self._toString(cs[0])
            if cs[0].type == 'FunCall':
                temp_str += Fore.RED + ';' + Fore.RESET
            if cs[0].type != 'StmtBlock' and cs[0].type != 'IfStmt' and cs[0].type != 'WhileStmt':
                temp_str += '\n'
            return temp_str
        
        elif node.type == 'ReturnStmt':
            return Fore.GREEN + "return" + Fore.RESET + " {0}".format(self._toString(cs[0])) + Fore.RED + ";" + Fore.RESET
        
        elif node.type == 'IfStmt':
            temp_str += Fore.GREEN + "if" + Fore.RESET + " ({0})\n".format(self._toString(cs[0]))
            if cs[1].children[0].type != 'StmtBlock':
                self._nr_tabs += 1
            temp_str += "{0}".format(self._toString(cs[1]))
            if cs[1].children[0].type != 'StmtBlock':
                self._nr_tabs -= 1
            if cs[2].children != []:
                temp_str += self._tabs() + Fore.GREEN + "else" + Fore.RESET + "\n"
                if cs[2].children[0].children[0].children[0].type != 'StmtBlock':
                    self._nr_tabs += 1
                temp_str += "{0}".format(self._toString(cs[2]))
                if cs[2].children[0].children[0].children[0].type != 'StmtBlock':
                    self._nr_tabs -= 1
            return temp_str
            
        elif node.type == 'ElseStmt':
            if cs != []:
                return self._toString(cs[0])
        
        elif node.type == 'WhileStmt':
            temp_str += Fore.GREEN + "while" + Fore.RESET + "({0})\n".format(self._toString(cs[0]))
            if cs[1].children[0].type != 'StmtBlock':
                self._nr_tabs += 1
            temp_str += "{0}".format(self._toString(cs[1]))
            if cs[1].children[0].type != 'StmtBlock':
                self._nr_tabs -= 1
            return temp_str
        
        elif node.type == 'StmtBlock':
            temp_str += "{\n"
            self._nr_tabs += 1
            for child in cs:
                temp_str += self._toString(child)
            self._nr_tabs -= 1
            temp_str += self._tabs() + "}\n"            
            return temp_str
        
        elif node.type == 'Assignment':
            return "{0} {2}={3} {1}".format(self._toString(cs[0]), self._toString(cs[1]), Fore.RED, Fore.RESET) + Fore.RED + ";" + Fore.RESET
            
        elif node.type == 'RetValue':
            if len(cs) == 1:
                return self._toString(cs[0])
        
        elif node.type == 'FunCall':
            return "{2}{0}{3}({1})".format(self._toString(cs[0]), self._toString(cs[1]), Fore.CYAN, Fore.RESET)
        
        elif node.type == 'ActArgs':
            if cs == []:
                return ""
            if len(cs) == 1:
                return self._toString(cs[0])
            else:
                return "{0}".format(self._toString(cs[0]))
            
        elif node.type == 'TypeList':
            return "[{0}]".format(self._toString(cs[0]))

        elif node.type == 'Exp':
            return "{0}{1}".format(self._toString(cs[0]), self._toString(cs[1]))

        elif node.type == 'ExpPrime*':
            for child in cs:
                temp_str += self._toString(child)
            return temp_str
        
        elif node.type == 'ExpPrime':
            return Fore.RED + " : " + Fore.RESET + "{0}".format(self._toString(cs[0])) + (self._toString(cs[1]) if len(cs)>1 else "")
        
        elif node.type == "Exp6":
            return " || ".join([self._toString(child) for child in cs])
        
        elif node.type == "Exp5":
            return " && ".join([self._toString(child) for child in cs])
        
        elif re.match("^Exp\\d", node.type):
            for child in cs:
                temp_str += self._toString(child)
            return temp_str
        
        elif node.type == "Op1Exp":
            for child in cs:
                temp_str += self._toString(child)
            return temp_str
        
        elif node.type == "Op1":
            return Fore.RED + cs[0].type + Fore.RESET
        
        elif re.match("^Op[2-4]", node.type):
            return " " + Fore.RED + cs[0].type + Fore.RESET + " "        
        
        elif node.type == "TupleExp":
            return "({0}, {1})".format(self._toString(cs[0]), self._toString(cs[1]))
        
        elif node.type == "ParenthesesExp":
            return "({0})".format(self._toString(cs[0]))
        
        else:
            print "you forgot to implement prettyPrinter._toString for type: {0}".format(node.type)
            return None
                
    def pprint(self, parse_tree):
        print self._toString(parse_tree)
        