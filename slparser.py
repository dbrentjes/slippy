from Tokenizer import Tokenizer

class SLParser:
    class Node:
        def __init__(self, type, children, token = None):
            self.type = type
            self.children = children
            self.token = token
        
        def _print(self, tabs):
            s = ("\t" * tabs) + self.type + "\n"
            for child in self.children:
                s = s + child._print(tabs+1)
                
            return s
        
        def __str__(self):
            return self._print(0)
        def __repr__(self):
            return self.__str__()
    
    class ParseException(Exception):
        def __init__(self, goal, token, expected):
            self._goal = goal
            self._token = token
            self._expected = expected
        def __str__(self):
            return "Error parsing " + self._goal + ". Expected '" + self._expected + "', read " + str(self._token)
    
    def __init__(self, tokenizer):
        self._tokens = []
        for token in tokenizer.tokenize():
            self._tokens.append(token)
        self._tokenIndex = 0

    def _readToken(self):
        """ consume one token """
        if self._tokenIndex >= len(self._tokens):
            print "reading out of bounds!"
            return Tokenizer.Token(0, 0, Tokenizer.Token.Type.__dict__['invalid'], None)
        token = self._tokens[self._tokenIndex]
        self._tokenIndex = self._tokenIndex + 1
        return token
    
    def _lookAhead(self):
        """ look ahead one token, give one token without consuming it """
        return self._tokens[self._tokenIndex]
    
    def _assertToken(self, expectedToken, goal):
        """ consume a token and throw an exception if it is not of given type """
        token = self._readToken()
        if not token.isType(expectedToken):
            raise SLParser.ParseException(goal, token, expectedToken)
        return token

    def parseProg(self):
        try:
            nodes = []
            while self._tokenIndex < len(self._tokens):
                node = self._parseDecl()
                nodes.append(node)
            return SLParser.Node("Prog", nodes)
        except SLParser.ParseException as e:
            print e

    def _parseDecl(self):
        #store token index for when we need to backtrack
        tokenIndex = self._tokenIndex
        try:
            node = self._parseVarDecl()
            return node
        except SLParser.ParseException as e:
            #reset token index
            self._tokenIndex = tokenIndex
            node = self._parseFuncDecl()
            return node

    def _parseVarDecl(self):
        nodeType = self._parseType()
        nodeId = self._parseId()
        
        equalsToken = self._assertToken('=', "VarDecl")
        nodeExp = self._parseExp()
        self._assertToken(';', "VarDecl")
        
        return SLParser.Node("VarDecl", [nodeType, nodeId, nodeExp], equalsToken)

    def _parseType(self):
        if self._lookAhead().isType('Int'):
            return self._parseTypeInt()
        elif self._lookAhead().isType('Bool'):
            return self._parseTypeBool()
        elif self._lookAhead().isType('('):
            return self._parseTypeTuple()
        elif self._lookAhead().isType('['):
            return self._parseTypeList()
        elif self._lookAhead().isType('id'):
            return self._parseTypeId()
        
        raise SLParser.ParseException("Type", self._lookAhead(), "type identifier",)
            
    def _parseTypeInt(self):
        token = self._assertToken('Int', "TypeInt")
        
        return SLParser.Node("Int", [], token)

    def _parseTypeBool(self):
        token = self._assertToken('Bool', "TypeBool")
        
        return SLParser.Node("Bool", [], token)

    def _parseTypeTuple(self):
        # consume '('
        self._assertToken('(', "TypeTuple")

        # parse first type
        firstType = self._parseType()
        
        # consume ','
        self._assertToken(',', "TypeTuple")
        
        # parse second type
        secondType = self._parseType()
        
        # consume ')'
        self._assertToken(')', "TypeTuple")
        
        return SLParser.Node("Tuple", [firstType, secondType])

    def _parseTypeList(self):
        # consume '['
        self._assertToken('[', "TypeList")
        
        # parse type
        nodeType = self._parseType()
        
        # consume ']'
        self._assertToken(']', "TypeList")
        
        return SLParser.Node("TypeList", [nodeType])

    def _parseTypeId(self):
        typeId = self._parseId()
        return SLParser.Node("TypeId", [typeId])
    
    def _parsePlus(self, parseFunction, nodeTitle):
        tokenIndex = self._tokenIndex
        node = parseFunction()
                
        nodes = []
        while node:
            try:
                nodes.append(node)
                tokenIndex = self._tokenIndex
                node = parseFunction()
            except SLParser.ParseException as e:
                break
        
        #last one will have failed, backtrack
        self._tokenIndex = tokenIndex
        
        return SLParser.Node(nodeTitle, nodes)
            
    
    def _parseStar(self, parseFunction, nodeTitle):
        """ Parse the kleene star. Executes parseFunction until it returns None
        Returns a node with title nodeTitle and children the results of parseFunction
        """
        tokenIndex = self._tokenIndex     
        nodes = []
        try:
            node = parseFunction()
            while node:
                #successfully parsed a node. Add it to the list
                nodes.append(node)
                
                #update the tokenindex, because the parsing up until now was successful
                tokenIndex = self._tokenIndex
                
                #try to parse another node
                node = parseFunction()
        except SLParser.ParseException as e:
            pass
        
        #parsing the last node was unsuccessful, backtrack
        self._tokenIndex = tokenIndex
        
        return SLParser.Node(nodeTitle, nodes)    
    
    def _parseOptional(self, parseFunction, nodeTitle):
        """ Parse an optional element of the grammar: [blaat])
        parseFunction: a function that parses blaat
        Return a node with title nodeTitle, and children [blaat] if it successfully parsed blaat, else []
        """
        tokenIndex = self._tokenIndex
        try:
            node = parseFunction()
            return SLParser.Node(nodeTitle, [node])
        except SLParser.ParseException:
            #parsing was not successful, backtrack
            self._tokenIndex = tokenIndex
            return SLParser.Node(nodeTitle, [])
    
    def _parseFArgsPrime(self):
        """ Parse ', FArgs'
        """
        self._assertToken(',', "FArgs'")
        return self._parseFArgs()
    
    def _parseFArgs(self):
        nodeType = self._parseType()
        
        token = self._tokens[self._tokenIndex]
        nodeId = self._parseId()
            
        nodeFArgs = self._parseOptional(self._parseFArgsPrime, "FArgs")
        
        retNode = SLParser.Node("FArgs", [nodeType, nodeId, nodeFArgs])
        retNode.token = token
        return retNode
    
    def _parseFuncDecl(self):
        tokenIndex = self._tokenIndex
        nodeRetType = self._parseRetType()
        
        token = self._tokens[self._tokenIndex]
        nodeId = self._parseId()
        self._assertToken('(', "FuncDecl")
        
        nodeFArgs = self._parseOptional(self._parseFArgs, "FArgs")
        
        self._assertToken(')', "FuncDecl")
        self._assertToken('{', "FuncDecl")
        
        nodeVarDecls = self._parseStar(self._parseVarDecl, "VarDecls")
        
        nodeStmts = self._parsePlus(self._parseStmt, "Stmts")
        
        self._assertToken('}', "FuncDecl")
        
        retNode = SLParser.Node("FuncDecl", [nodeRetType, nodeId, nodeFArgs, nodeVarDecls, nodeStmts])        
        retNode.token = token
        return retNode
    
    def _parseRetType(self):
        if self._lookAhead().isType('Void'):
            return SLParser.Node("RetType", [self._parseVoid()])
        else:
            return SLParser.Node("RetType", [self._parseType()])
    
    def _parseVoid(self):
        token = self._assertToken('Void', "Void")
        return SLParser.Node("Void", [], token)
    
    def _parseExpOne(self):
        #store token index for when we need to backtrack
        tokenIndex = self._tokenIndex
        if self._lookAhead().isType('id'):
            #this is either a funcall or an id
            try:
                return SLParser.Node("Exp1", [self._parseFunCall()])
            except SLParser.ParseException:
                self._tokenIndex = tokenIndex
                return SLParser.Node("Exp1", [self._parseId()])
        elif self._lookAhead().isType('Op1'):
            return SLParser.Node("Exp1", [self._parseOpOneExp()])
        elif self._lookAhead().isType('-'):
            return SLParser.Node("Exp1", [self._parseOpOneExp()])
        elif self._lookAhead().isType('int'):
            return SLParser.Node("Exp1", [self._parseInt()])
        elif self._lookAhead().isType('boolConst'):
            return SLParser.Node("Exp1", [self._parseBool()])
        elif self._lookAhead().isType('('):
            #this is either (Exp) or (Exp, Exp)
            tokenIndex = self._tokenIndex
            try:
                return SLParser.Node("Exp1", [self._parseTupleExp()])
            except SLParser.ParseException:
                self._tokenIndex = tokenIndex
                return SLParser.Node("Exp1", [self._parseParenthesesExp()])
        elif self._lookAhead().isType('['):
            return SLParser.Node("Exp1", [self._parseEmptyList()])
        else:
            raise SLParser.ParseException("ExpOne", self._lookAhead(), "ExpOne")

    def _parseExpTwo(self):
        nodeExpOne = self._parseExpOne()
        nodeExpRest = self._parseStar(self._parseExpTwoPrime, "Exp2Prime*")
        if nodeExpRest.children:
            return SLParser.Node("Exp2", [nodeExpOne, nodeExpRest])
        else:
            return nodeExpOne
    
    def _parseExpTwoPrime(self):
        nodeOp = self._parseOpTwo()        
        nodeExpOne = self._parseExpOne()
        return SLParser.Node("Exp2Prime", [nodeOp, nodeExpOne])
    
    def _parseExpThree(self):
        nodeExpTwo = self._parseExpTwo()
        nodeExpRest = self._parseStar(self._parseExpThreePrime, "Exp3Prime*")
        if nodeExpRest.children:
            return SLParser.Node("Exp3", [nodeExpTwo, nodeExpRest])
        else:
            return nodeExpTwo
    
    def _parseExpThreePrime(self):
        nodeOp = self._parseOpThree()
        nodeExp = self._parseExpTwo()
        return SLParser.Node("Exp3Prime", [nodeOp, nodeExp])
    
    def _parseExpFour(self):
        nodeExpThree = self._parseExpThree()
        nodeExpRest = self._parseStar(self._parseExpFourPrime, "Exp4Prime*")
        if nodeExpRest.children:
            return SLParser.Node("Exp4", [nodeExpThree, nodeExpRest])
        else:
            return nodeExpThree
    
    def _parseExpFourPrime(self):
        nodeOp = self._parseOpFour()
        nodeExp = self._parseExpThree()
        return SLParser.Node("Exp4Prime", [nodeOp, nodeExp])
    
    def _parseExpFive(self):
        nodeExp = self._parseExpFour()
        nodeExpRest = self._parseStar(self._parseExpFivePrime, "Exp5Prime*")
        if nodeExpRest.children:
            return SLParser.Node("Exp5", [nodeExp, nodeExpRest])
        else:
            return nodeExp
    
    def _parseExpFivePrime(self):
        self._assertToken('&&', "Exp5'")
        nodeExp = self._parseExpFour()
        return SLParser.Node("Exp5Prime", [nodeExp])
    
    def _parseExpSix(self):
        nodeExpFive = self._parseExpFive()
        nodeExpRest = self._parseStar(self._parseExpSixPrime, "Exp6Prime*")
        if nodeExpRest.children:
            return SLParser.Node("Exp6", [nodeExpFive, nodeExpRest])
        else:
            return nodeExpFive

    def _parseExpSixPrime(self):
        self._assertToken('||', "Exp6'")
        nodeExp = self._parseExpFive()
        return SLParser.Node("Exp6Prime", [nodeExp])
    
    def _parseExp(self):
        nodeExpSix = self._parseExpSix()
        if self._lookAhead().isType(':'):
            expRest = self._parseExpPrime()
            return SLParser.Node("Exp", [nodeExpSix, expRest])
        else:
            return nodeExpSix
        
    def _parseExpPrime(self):
        self._assertToken(':', "Exp'")
        nodeExp = self._parseExpSix()
        if self._lookAhead().isType(':'):
            expRest = self._parseExpPrime()
            return SLParser.Node("ExpPrime", [nodeExp, expRest])
        else:
            return SLParser.Node("ExpPrime", [nodeExp])
    
    def _parseParenthesesExp(self):
        self._assertToken('(', "ParenthesesExp")
        nodeExp = self._parseExp()
        self._assertToken(')', "ParenthesesExp")
        return SLParser.Node("ParenthesesExp", [nodeExp])
    
    def _parseOpOne(self):
        token = self._readToken()
        if token.isType('Op1'):
            return SLParser.Node("Op1", [SLParser.Node(token.id_name, [], token)])    
        elif token.isType('-'):
            return SLParser.Node("Op1", [SLParser.Node('-', [], token)])         
        else:
            raise SLParser.ParseException("Op1", token, "Op1")      
    
    def _parseOpOneExp(self):
        nodeOp = self._parseOpOne()
        nodeExp = self._parseExpOne()
        return SLParser.Node("Op1Exp", [nodeOp, nodeExp])
    
    def _parseId(self):
        token = self._assertToken('id', "id")
        return SLParser.Node("id", [SLParser.Node(token.id_name, [], token)])
        
    def _parseInt(self):
        token = self._assertToken('int', "int")
        
        return SLParser.Node("int", [SLParser.Node(token.id_name, [], token)])
    
    def _parseBool(self):
        token = self._assertToken('boolConst', "bool")
        return SLParser.Node("bool", [SLParser.Node(token.id_name, [], token)])

    def _parseFunCall(self):
        nodeId = self._parseId()
        self._assertToken('(', "FunCall")
        nodeActArgs = self._parseOptional(self._parseActArgs, "ActArgs")
        self._assertToken(')', "FunCall")
        return SLParser.Node("FunCall", [nodeId, nodeActArgs])
    
    def _parseActArgs(self):
        nodeExp = self._parseExp()
        nodeActArgs = self._parseOptional(self._parseActArgsPrime, "ActArgs")
        return SLParser.Node("ActArgs", [nodeExp, nodeActArgs])
    
    def _parseActArgsPrime(self):
        """ parse ',' ActArgs """
        self._assertToken(',', "ActArgsPrime")
        return self._parseActArgs()
    
    def _parseEmptyList(self):
        """ parse '[]' """
        self._assertToken('[', "EmptyList")
        self._assertToken(']', "EmptyList")
        return SLParser.Node("EmptyList", [])
        
    def _parseTupleExp(self):
        self._assertToken('(', "TupleExp")
        expA = self._parseExp()
        self._assertToken(',', "TupleExp")
        expB = self._parseExp()
        self._assertToken(')', "TupleExp")
        return SLParser.Node("TupleExp", [expA, expB])
    
    def _parseOpTwo(self):
        token = self._assertToken('Op2', "Op2")
        return SLParser.Node("Op2", [SLParser.Node(token.id_name, [], token)])

    def _parseOpThree(self):
        token = self._readToken()
        if token.isType('Op3'):
            return SLParser.Node("Op3", [SLParser.Node(token.id_name, [], token)])
        elif token.isType('-'):
            return SLParser.Node("Op3", [SLParser.Node('-', [], token)])
        else:
            raise SLParser.ParseException("Op3", token, 'Op3')
        
    def _parseOpFour(self):
        token = self._assertToken('Op4', "Op4")        
        return SLParser.Node("Op4", [SLParser.Node(token.id_name, [], token)])
    
    def _parseStmt(self):
        if self._lookAhead().isType('{'):
            return SLParser.Node("Stmt", [self._parseStmtBlock()])
        elif self._lookAhead().isType('if'):
            return SLParser.Node("Stmt", [self._parseIfStmt()])
        elif self._lookAhead().isType('while'):
            return SLParser.Node("Stmt", [self._parseWhile()])
        elif self._lookAhead().isType('return'):
            return SLParser.Node("Stmt", [self._parseReturn()])
        elif self._lookAhead().isType('id'):
            #This is either a FunCall or assignment
            tokenIndex = self._tokenIndex
            try:
                node = SLParser.Node("Stmt", [self._parseFunCall()])
                self._assertToken(';', "FunCall")
                return node
            except SLParser.ParseException:
                self._tokenIndex = tokenIndex
                return SLParser.Node("Stmt", [self._parseAssignment()])
        else:
            raise SLParser.ParseException("Stmt", self._lookAhead(), 'statement')       
    
    def _parseStmtBlock(self):
        self._assertToken('{', "StmtBlock")
        node = self._parseStar(self._parseStmt, "StmtBlock")
        self._assertToken('}', "StmtBlock")
        return node
    
    def _parseIfStmt(self):
        ifToken = self._assertToken('if', "IfStmt")
        self._assertToken('(', "IfStmt")
        nodeIf = self._parseExp()
        self._assertToken(')', "IfStmt")
        nodeStmt = self._parseStmt()      
        nodeElse = self._parseOptional(self._parseElseStmt, "ElseStmt")
        return SLParser.Node("IfStmt", [nodeIf, nodeStmt, nodeElse], ifToken)
        
    def _parseElseStmt(self):
        elseToken = self._assertToken('else', "ElstStmt")
        nodeStmt = self._parseStmt()
        return SLParser.Node("ElseStmt", [nodeStmt], elseToken)
    
    def _parseWhile(self):
        whileToken = self._assertToken('while', "WhileStmt")
        self._assertToken('(', "WhileStmt")
        nodeCond = self._parseExp()
        self._assertToken(')', "WhileStmt")
        nodeStmt = self._parseStmt()
        return SLParser.Node("WhileStmt", [nodeCond, nodeStmt], whileToken)
    
    def _parseAssignment(self):
        nodeId = self._parseId()
        equalsToken = self._assertToken('=', "Assignment")
        nodeExp = self._parseExp()
        self._assertToken(';', "Assignment")
        return SLParser.Node("Assignment", [nodeId, nodeExp], equalsToken)
    
    def _parseReturn(self):
        token = self._assertToken('return', "ReturnStmt")
        nodeExp = self._parseOptional(self._parseExp, "RetValue")
        self._assertToken(';', "ReturnStmt")
        return SLParser.Node("ReturnStmt", [nodeExp], token)
