from type import *

class Context:
    def __init__(self, context=None):
        # Larger scope context
        self._superContext = context
        
        # Create empty dict
        self._id_dict = {}
        
    def addPredefineds(self):
        # add predefined functions
        self.declareType("fst", FunctionType([TupleType(GenericType("a"), GenericType("b"))], GenericType("a")))
        self.declareType("first", FunctionType([TupleType(GenericType("a"), GenericType("b"))], GenericType("a")))
        self.declareType("snd", FunctionType([TupleType(GenericType("c"), GenericType("d"))], GenericType("d")))
        self.declareType("head", FunctionType([ListType(GenericType("e"))], GenericType("e")))
        self.declareType("tail", FunctionType([ListType(GenericType("f"))], ListType(GenericType("f"))))
        self.declareType("isEmpty", FunctionType([ListType(GenericType("g"))], Type(Type.BOOL)))
        self.declareType("print", FunctionType([GenericType("h")], Type(Type.VOID)))
        
    def __str__(self):
        ret = ""
        for id in self._id_dict:
            ret = ret +  id + ": " + str(self._id_dict[id]) + "\n"
        if self._superContext:
            ret += "\nsupercontext:\n" + str(self._superContext)
        return ret
    
    class MultiplyDefinedException(Exception):
        
        def __init__(self, id, type):
            self.id = id
            self.type = type
            
    class UndefinedIdException(Exception):
        def __init__(self, id):
            self.id = id
    
    def assert_is_defined(self, id):
        if not self.isDefined(id):
            raise Context.UndefinedIdException(id)
    
    def isDefined(self, id):
        """ check whether an id is defined in the current scope or higher scope """
        if id in self._id_dict:
            return True
        elif self._superContext and self._superContext.isDefined(id):
            return True
        else:
            return False
    
    def getType(self, id):
        """ get the type of given id, from the local scope or 
            higher scope when it's not defined in local scope """
        if id in self._id_dict:
            return self._id_dict[id]
        elif self._superContext:
            return self._superContext.getType(id)
        else:
            raise Context.UndefinedIdException(id)
    
    def declareType(self, id, type):
        """ declare the type of an id in the local scope """
        if id in self._id_dict:
            raise Context.MultiplyDefinedException(id, type)
        else:
            self._id_dict[id] = type
    
    def substitute(self, genericType, actualType):
        #print "substituting " + str(genericType) + " for " + str(actualType)
        """ substitute all occurences of a given genericType with actualType """
        for declaredType in self._id_dict:
            type = self.getType(declaredType)
            if type == genericType:
                self._id_dict[declaredType] = actualType
            else:
                type.substitute(genericType, actualType)
        
        if self._superContext:
            self._superContext.substitute(genericType, actualType)
                
